﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Class
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 CourseId { get; set; }
        public Int16 ScheduleId { get; set; }
        public Int16 DepartmentId { get; set; }
        
        #endregion

        #region Navigation Properties
        public virtual ICollection<Course> Courses { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<ScheduleColumn> ScheduleColumns { get; set; }
        public virtual ICollection<ApplicationUser> Students { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ApplicationUser Teacher { get; set; }
        #endregion
    }
}
