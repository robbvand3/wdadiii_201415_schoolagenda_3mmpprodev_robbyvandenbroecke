﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
       public enum PostAudience
       {
           Everybody = 0,
           Students = 1,
           Lecturers = 2,
           Staff = 3,
           Alumni = 4,
           ExternalCompanies = 5
       }

       public class Blog
       {
            #region Properties
            public int Id { get; set; }
            public string Title { get; set; }
            public string Synopsis { get; set; }
            public string Content { get; set; }
            public Byte[] Created { get; set; }
            public DateTime? Updated { get; set; }
            public DateTime? Deleted { get; set; }
            #endregion

            #region Foreign Keys
            public Int16? DepartmentId { get; set; }
            #endregion

            #region Navigation Properties
            public virtual ICollection<Category> Categories { get; set; }
            public virtual ICollection<Tag> Tags { get; set; }
            public virtual ICollection<Comment> Comments { get; set; }
            public virtual Department Department { get; set; }
            #endregion
        }
    }

