﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Course
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 DepartmentId { get; set; }
        public Int16 ClassId { get; set; }
        public int? TeacherId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<ScheduleRow> ScheduleRows { get; set; }
        public virtual ICollection<ReportcardRow> ReportcardRows { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ApplicationUser Teacher { get; set; }
        #endregion
    }
}
