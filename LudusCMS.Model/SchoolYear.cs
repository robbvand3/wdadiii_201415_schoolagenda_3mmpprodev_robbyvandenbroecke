﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class SchoolYear
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime SchoolYearStart { get; set; }
        public DateTime SchoolYearEnd { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<Department> Departments { get; set; }
        #endregion
    }
}
