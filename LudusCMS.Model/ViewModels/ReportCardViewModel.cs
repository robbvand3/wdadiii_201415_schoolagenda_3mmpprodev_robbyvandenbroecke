﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ReportCardViewModel
    {
        public Reportcard Reportcard { get; set; }
        public IEnumerable<ApplicationUser> ApplicationUsers { get; set; }

        public ApplicationUser User { get; set; }
        public IEnumerable<ReportcardRow> ReportcardRows { get; set; }
        public IEnumerable<Course> Courses { get; set; }
    }
}