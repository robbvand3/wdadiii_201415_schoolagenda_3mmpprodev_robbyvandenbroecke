﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ScheduleRowViewModel
    {
        public ScheduleRow ScheduleRow { get; set; }
        public ScheduleColumn ScheduleColumn { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public Course Course { get; set; }
        public IEnumerable<Classroom> Classrooms { get; set; }
        public Classroom Classroom { get; set; }
    }
}