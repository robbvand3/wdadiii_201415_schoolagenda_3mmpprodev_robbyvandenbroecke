﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class CreateCommentForBlogViewModel
    {
        [Required]
        public Comment Comment { get; set; }
        [Required]
        public int BlogId { get; set; }
        public int? UserId { get; set; }
        public int? ParentCommentId { get; set; }
        public string UserAvatar { get; set; }
        public string AJAXUpdateTargetId { get; set; }
    }
}
