﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LudusCMS.Model.ViewModels
{
    public class BlogViewModel
    {
        public Blog Blog { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public int[] SelectedTagIds { get; set; }
        public IEnumerable<SelectListItem> Tags { get; set; }
        public int[] SelectedCategoryIds { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        public IEnumerable<PostAudience> Audiences { get; set; }
    }
}
