﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ScheduleViewModel
    {
        public Schedule Schedule { get; set; }
        public IEnumerable<ScheduleRow> ScheduleRow { get; set; }
        public IEnumerable<ScheduleColumn> ScheduleColumn { get; set; }
        public IEnumerable<Class> Classes { get; set; }
    }
}
