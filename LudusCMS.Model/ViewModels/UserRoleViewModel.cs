﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.Viewmodels
{
    public class UserRoleViewModel
    {
        public ApplicationUserRole userrole { get; set; }

        public IEnumerable<ApplicationUser> users { get; set; }
        

        public IEnumerable<ApplicationRole> roles { get; set; }
    }
}
