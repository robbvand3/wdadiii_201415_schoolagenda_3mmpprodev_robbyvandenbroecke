﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class CourseViewModel
    {
        public Course course { get; set; }
        public IEnumerable<Event> events { get; set; }
        public IEnumerable<Class> klassen {get; set;}
        public IEnumerable<Department> Departments { get; set; }
        public IEnumerable<ApplicationUser> Teachers { get; set; }
    }
}
