﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
   public class ClassViewModel
    {
        public Class Class { get; set; }
        public IEnumerable<Class> Classes { get; set; }
        public IEnumerable<Department> Departments { get; set; }
    }
}
