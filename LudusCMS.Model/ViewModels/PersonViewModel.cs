﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LudusCMS.Model.ViewModels
{
    public class PersonViewModel
    {
        public Person Person { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public IEnumerable<ApplicationRole> Roles { get; set; }
        
    }
}
