﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.Viewmodels
{
    public class AmountForEntity
    {
        public int Amount { get; set; }
        public string EntityType { get; set; }
        public string EntityTypePlural { get; set; }
    }
}
