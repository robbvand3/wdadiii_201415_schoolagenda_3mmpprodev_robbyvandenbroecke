﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace LudusCMS.Model.Viewmodels
{
    public class UserRegisterViewModel
    {
        [Required]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Username")]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [StringLength(128, MinimumLength = 2, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(128, MinimumLength = 2, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Required]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and the confirmation password do not match!")]
        public string ConfirmPassword { get; set; }
        [Required]
        [Display(Name = "Role :")]

        public int Id { get; set; }
        public string Name { get; set; }
        public Int16? DepartmentId { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public int? RoleId { get; set; }

        public ApplicationUser User { get; set; }
        
    }
}
