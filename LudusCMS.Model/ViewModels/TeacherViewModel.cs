﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class TeacherViewModel
    {
        public ApplicationUser CurrentTeacher { get; set; }
        public IEnumerable<Class> Classes { get; set; }
    }
    
}
