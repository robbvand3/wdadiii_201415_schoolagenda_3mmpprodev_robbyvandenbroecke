﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LudusCMS.Model.ViewModels
{
    public class EventViewModel
    {
        public Event Event { get; set; }
        public IEnumerable<EventCategory> EventCategories { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public IEnumerable<Class> Classes { get; set; }
        public IEnumerable<Department> Departments { get; set; }

       
    }
}
