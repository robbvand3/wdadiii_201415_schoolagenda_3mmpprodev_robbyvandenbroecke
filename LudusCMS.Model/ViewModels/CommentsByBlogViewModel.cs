﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class CommentsByBlogViewModel
    {
        public IPagedList<Comment> Comments { get; set; }
        public int BlogId { get; set; }
        public int? UserId { get; set; }
    }
}
