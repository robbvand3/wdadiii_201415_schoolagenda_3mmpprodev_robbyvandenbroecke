﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class StudentViewModel
    {
        public ApplicationUser CurrentStudent { get; set; }
        public IEnumerable<Class> Classes { get; set; }
        public IEnumerable<ApplicationUser> AllStudents { get; set; }
    }
}
