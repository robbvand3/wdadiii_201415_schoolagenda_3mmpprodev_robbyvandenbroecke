﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LudusCMS.Model.ViewModels
{
    public class MessageViewModel
    {
        public Message Message { get; set; }
        public IEnumerable<ApplicationUser> Receivers { get; set; }
        public ApplicationUser ReceiverId { get; set; }
        
    }
}
