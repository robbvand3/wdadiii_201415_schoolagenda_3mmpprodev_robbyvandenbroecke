﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ClassroomViewModel
    {
        public Classroom Classroom { get; set; }
        public IEnumerable<Classroom> Classrooms { get; set; }
    }
}
