﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ScheduleColumnViewModel
    {
        public ScheduleColumn ScheduleColumn { get; set; }
        public Schedule Schedule { get; set; }
        public IEnumerable<Class> Classes { get; set; }
        public Class Class { get; set; }
        public IEnumerable<ScheduleRow> ScheduleRow { get; set; }
        public IEnumerable<Classroom> Classrooms { get; set; }
        public IEnumerable<Course> Courses { get; set; }
    }
}