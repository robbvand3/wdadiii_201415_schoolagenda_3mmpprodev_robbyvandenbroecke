﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LudusCMS.Model.ViewModels
{
    public class ReportcardRowViewModel
    {
        public ReportcardRow ReportcardRow { get; set; }
        public Reportcard Reportcard { get; set; }
        public IEnumerable<Course> Courses { get; set; }
       
    }
}