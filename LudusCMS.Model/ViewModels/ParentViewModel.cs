﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model.ViewModels
{
    public class ParentViewModel
    {
        public ApplicationUser CurrentParent { get; set; }
        public IEnumerable<ApplicationUser> AllStudents { get; set; }
    }
}
