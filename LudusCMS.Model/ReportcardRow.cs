﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class ReportcardRow
    {
        #region Properties
        public Int16 Id { get; set; }
        public decimal TaskScore { get; set; }
        public decimal TaskPoint { get; set; }
        public decimal TaskTotal { get; set; }
        public decimal TestScore { get; set; }
        public decimal TestPoint { get; set; }
        public decimal TestTotal { get; set; }
        public decimal TotalScore { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        public string Description { get; set; }
        #endregion

        #region Foreign Keys
        public Int16? CourseId { get; set; }
        public Int16? ReportcardId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual Reportcard Reportcard { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        #endregion

        
    }
}

