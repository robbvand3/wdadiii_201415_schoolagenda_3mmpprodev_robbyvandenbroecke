﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Teacher :Person
    {
        public string LecturerNumber { get; set; }
        public string LecturerEmail { get; set; }
        public string LecturerPicture { get; set; }
    }
}
