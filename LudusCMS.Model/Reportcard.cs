﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LudusCMS.Model
{
    public class Reportcard
    {

        #region Properties
        public Int16 Id { get; set; }
        public int Signature1 { get; set; }
        public int Signature2 { get; set; }
        public string Title { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public int UserId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<ReportcardRow> ReportcardRows { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ApplicationUser User { get; set; }
        #endregion
    }
}
