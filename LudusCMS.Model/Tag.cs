﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Tag
    {
        #region PROPERTIES
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<Blog> Blogs { get; set; }
        #endregion
    }
}
