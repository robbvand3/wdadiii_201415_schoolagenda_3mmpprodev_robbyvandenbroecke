﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Comment
    {
        #region Properties
        public Int64 Id { get; set; }
        public string Content { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public int BlogId { get; set; }
        public Int64? ParentCommentId { get; set; }
        public int? UserId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual Blog Blog { get; set; }
        public Comment ParentComment { get; set; }
        public ICollection<Comment> ChildComments { get; set; }
        public virtual ApplicationUser User { get; set; }
        #endregion
    }
}
