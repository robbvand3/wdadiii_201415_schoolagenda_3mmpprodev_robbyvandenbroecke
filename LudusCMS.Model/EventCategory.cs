﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class EventCategory
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        #endregion

        #region Navigation Properties
        public virtual ICollection<Event> Events { get; set; }
        #endregion
    }
}
