﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Message
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }

       
        #endregion

        #region Navigation Properties
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<ApplicationUser> Receivers { get; set; }
        #endregion

    }
}
