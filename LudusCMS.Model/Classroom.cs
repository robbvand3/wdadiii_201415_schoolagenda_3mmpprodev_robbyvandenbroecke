﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Classroom
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 ScheduleRowId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual ICollection<ScheduleRow> ScheduleRows { get; set; }
        #endregion
    }
}
