﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Department
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 SchoolYearId { get; set; }
        public Int16 CourseId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual SchoolYear SchoolYear { get; set; }
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Course> Courses { get; set; }

        public virtual ICollection<Class> Classes { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<ApplicationUser> Person { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        
        #endregion
    }
}
