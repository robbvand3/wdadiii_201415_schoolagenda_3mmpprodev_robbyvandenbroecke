﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class ScheduleColumn
    {
       #region Properties
        public Int16 Id { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 ClassId { get; set; }
        public Int16 ScheduleId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual Class Class { get; set; }
        public virtual Schedule Schedule { get; set; }
        public virtual ICollection<ScheduleRow> ScheduleRows { get; set; }
        #endregion
    }
}
