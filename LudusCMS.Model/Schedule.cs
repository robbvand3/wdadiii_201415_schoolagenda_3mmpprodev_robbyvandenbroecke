﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Schedule
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region navigation properties
        public virtual ICollection<ScheduleColumn> ScheduleColumns { get; set; }
        #endregion 
    }
}
