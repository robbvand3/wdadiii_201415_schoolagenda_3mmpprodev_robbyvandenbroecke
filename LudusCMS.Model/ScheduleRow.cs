﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class ScheduleRow
    {
        #region Properties
        public Int16 Id { get; set; }
        public int Position { get; set; }
        public string Description { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16 CourseId { get; set; }
        public Int16 ScheduleColumnId { get; set; }
        public Int16 ClassroomId { get; set; }
        #endregion

        #region Navigation Properties
        public virtual Course Course { get; set; }
        public virtual ScheduleColumn ScheduleColumn { get; set; }
        public virtual ICollection<Classroom> Classrooms { get; set; }
        #endregion
    }
}
