﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        #region Extra Properties
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        #endregion

        public Int16? ClassId { get; set; }
        public int? ChildId { get; set; }
        public Int16? DepartmentId { get; set; }
        

        #region Navigation Properties
       
        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Reportcard> Reportcards { get; set; }
        public virtual ICollection<ApplicationUser> Parents { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual Class Class { get; set; }
        public virtual ApplicationUser Child { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        #endregion

        #region Generate my own Identity
        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = manager.CreateIdentity<ApplicationUser, int>(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        #endregion
    }
}
