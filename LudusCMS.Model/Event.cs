﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Event
    {
        #region Properties
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public Byte[] Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Deleted { get; set; }
        #endregion

        #region Foreign Keys
        public Int16? EventCategoryId { get; set; }
        public Int16? CourseId { get; set; }
        public Int16? DepartmentId { get; set; }
        public Int16? ClassId { get; set; }
        public bool SignedFather { get; set; }
        public bool SignedMother { get; set; }
        #endregion

        #region Navigation Properties
        public virtual EventCategory EventCategory { get; set; }
        public virtual Course Course { get; set; }
        public virtual Class Class { get; set; }
        public virtual Department Department { get; set; }
        #endregion
    }
}
