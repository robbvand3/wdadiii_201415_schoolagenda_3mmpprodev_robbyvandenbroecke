﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LudusCMS.Model
{
    public class Student : Person
    {
        public string StudentNumber { get; set; }
        public string StudentEmail { get; set; }
        public string StudentPicture { get; set; }
    }
}
