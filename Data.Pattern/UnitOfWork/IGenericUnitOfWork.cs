﻿using System;
namespace Data.Pattern.UnitOfWork
{
    interface IGenericUnitOfWork
    {
        void BeginTransaction(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.Unspecified);
        bool Commit();
        void Dispose();
        void Dispose(bool disposing);
        global::Data.Pattern.Repositories.IGenericRepository<T> Repository<T>() where T : class;
        global::Data.Pattern.Repositories.IGenericRepository<T> RepositoryAsync<T>() where T : class;
        void Rollback();
        int SaveChanges();
        System.Threading.Tasks.Task<int> SaveChangesAsync();
        System.Threading.Tasks.Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken);
    }
}
