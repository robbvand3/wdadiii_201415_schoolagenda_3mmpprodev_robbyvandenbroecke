﻿using System;
namespace Data.Pattern.Repositories
{
    public interface IGenericRepository<T>
     where T : class
    {
        void Delete(object id);
        void Delete(T entity);
        System.Threading.Tasks.Task<bool> DeleteAsync(object id);
        System.Linq.IQueryable<T> GetAll();
        System.Linq.IQueryable<T> GetBy(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, Func<System.Linq.IQueryable<T>, System.Linq.IOrderedQueryable<T>> orderBy = null, System.Collections.Generic.List<System.Linq.Expressions.Expression<Func<T, object>>> includes = null, int? page = null, int? pageSize = null);
        System.Threading.Tasks.Task<System.Linq.IQueryable<T>> GetByAsync(System.Linq.Expressions.Expression<Func<T, bool>> query = null, Func<System.Linq.IQueryable<T>, System.Linq.IOrderedQueryable<T>> orderBy = null, System.Collections.Generic.List<System.Linq.Expressions.Expression<Func<T, object>>> includes = null, int? page = null, int? pageSize = null);
        T GetById(object id);
        System.Threading.Tasks.Task<T> GetByIdAsync(object id);
        void Insert(T entity);
        void Save();
        void Update(T entity);
        void VirtualDelete(object id);
        void VirtualDelete(T entity);
        void VirtualUnDelete(object id);
        void VirtualUnDelete(T entity);
    }
}
