﻿var menuClicker = $('.submenu > a');
var arrowMenu = $('.submenu > a > .arrow');
var closed_check = 1;

function clickMenu() {
    menuClicker.on('click', function (ev) {
        ev.preventDefault();
        if (closed_check == 1) {
            $(this).find('.arrow').removeClass('glyphicon-chevron-right');
            $(this).find('.arrow').addClass('glyphicon-chevron-down');
            var slider = $(this).next('.slider-down');
            closed_check = 0;
        } else {
            $(this).find('.arrow').addClass('glyphicon-chevron-right');
            $(this).find('.arrow').removeClass('glyphicon-chevron-down');
            var slider = $(this).next('.slider-down');
            closed_check = 1;

        }
        var slider = $(this).next('.slider-down');
        slider.slideToggle(400);
        return false;
    })
}


function drawArrows() {
    arrowMenu.addClass('glyphicon glyphicon-chevron-right');
}




$(function () {
    clickMenu();
    drawArrows();
});