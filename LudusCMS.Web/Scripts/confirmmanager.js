﻿/*
 * Programmed by Philippe De Pauw -Waterschoot
 * Version: 1.0
 * Last updated: 17-10-2014
 * Education license: only use in educationel institutions!
 */
var ConfirmManager = (function ($) {

    var activeLinkCategory;
    var activeMessage;
    var activeLinkHref;
    var activeAction;
    var updateTarget;
    var updateLinkHref;

    var confirmLinkClass2;
    var confirmDialogId;
    var self;

    ConfirmManager.prototype.constructor = ConfirmManager;
    function ConfirmManager(linkClass, dialogId) {
        confirmLinkClass = linkClass;
        confirmDialogId = dialogId;
        self = this;
    }

    ConfirmManager.prototype.registerModalHandlers = function () {
        $(confirmDialogId).find('.btn-confirm').click(function (ev) {
            ev.preventDefault();

            console.log('click' + activeLinkHref);

            $.ajaxPrefilter(
               function (options, localOptions, jqXHR) {
                   if (options.type !== "GET") {
                       var token = GetAntiForgeryToken(updateTarget);
                       if (token !== null) {
                           options.data = "X-Requested-With=XMLHttpRequest" + (options.data === "") ? "" : "&" + options.data;
                           options.data = options.data + "&" + token.name + '=' + token.value;
                       }
                   }
               }
           );

            $.ajax({
                url: activeLinkHref,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.state === 1) {

                        if (window.AlertManager) {
                            window.AlertManager.showAlert('Success', data.message);
                        }

                        var row = $('tr[data-id="' + activeLinkCategory + '-' + data.id + '"]');

                        switch (activeAction) {
                            case 'delete':
                                $(row).animate({ opacity: 0.4 }, 680, function () {
                                    $(this).remove();

                                    $.get(updateLinkHref, function (data) {
                                        $(updateTarget).html(data);


                                        self.registerClickHandlers();
                                    });
                                });
                                break;
                            case 'lock': case 'unlock': case 'virtualdelete': case 'virtualundelete': case 'disabletwofactor': case 'enabletwofactor':
                                $(row).animate({ opacity: 0.4 }, 680, function () {

                                    $.get(updateLinkHref, function (data) {
                                        $(updateTarget).html(data);

                                        self.registerClickHandlers();
                                    });
                                });
                                break;
                        }

                    } else {
                        if (window.AlertManager) {
                            window.AlertManager.showAlert('Error', data.message);
                        }
                    }
                }
            });

            $('#confirm-dialog').modal('hide');

            return false;
        });
    }

    ConfirmManager.prototype.registerClickHandlers = function () {
        $(confirmLinkClass).click(function (ev) {
            ev.preventDefault();

            activeLinkCategory = $(this).data('cat');
            activeAction = $(this).data('action');
            activeLinkHref = $(this)[0].href;
            updateTarget = $(this).data('update-target');
            updateLinkHref = $(this).data('update-href');

            var id = $(this).data('id');
            var row = $('tr[data-id="' + activeLinkCategory + '-' + id + '"]');
            var title = $(row).data('title');

            switch (activeAction) {
                case 'delete':
                    activeMessage = "Are you sure you want to delete the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'lock':
                    activeMessage = "Are you sure you want to lock the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'unlock':
                    activeMessage = "Are you sure you want to unlock the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'virtualdelete':
                    activeMessage = "Are you sure you want to soft-delete the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'virtualundelete':
                    activeMessage = "Are you sure you want to soft-undelete the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'enabletwofactor':
                    activeMessage = "Are you sure you want to enable the Two Factor Auth for the selected " + activeLinkCategory + ": " + title + "?";
                    break;
                case 'disabletwofactor':
                    activeMessage = "Are you sure you want to disable the Two Factor Auth for the selected " + activeLinkCategory + ": " + title + "?";
                    break;
            }

            $(confirmDialogId).find('.modal-body').html(activeMessage);
            $(confirmDialogId).modal('show');

            return false;
        });
    };

    return ConfirmManager;
})($);

/*
 * Create a new ConfirmManager for usage in Views
 */
(function () {
    window.ConfirmManager = new ConfirmManager('.confirm-link', '#confirm-dialog');
})();