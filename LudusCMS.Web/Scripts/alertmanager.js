﻿/*
 * Programmed by Philippe De Pauw -Waterschoot
 * Version: 1.0
 * Last updated: 17-10-2014
 * Education license: only use in educational institutions!
 */
var AlertManager = (function ($) {

    var _alertContainer;

    function AlertManager(alertContainerId) {
        _alertContainer = $(alertContainerId);
    }

    AlertManager.prototype.showAlert = function (type, message) {
        var content = ''
        + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
        + '<div class="alert ' + convertAlertTypeToBootstrapAlertType(type) + '">'
        + '<p>' + message + '</p>'
        + '</div>'
        + '</div>';
        _alertContainer.append(content);

        var alertElement = _alertContainer.children().last();

        window.setTimeout(function () {
            alertElement.fadeOut('slow', function () {
                $(this).remove();
            });
        }, 3000);
    };

    function convertAlertTypeToBootstrapAlertType(altertType) {
        switch (altertType) {
            case 'Success': default: return 'alert-success';
            case 'Info': return 'alert-info';
            case 'Warning': return 'alert-warning';
            case 'Error': return 'alert-danger';
        }
    };

    return AlertManager;
})($);

(function () {
    window.AlertManager = new AlertManager('#alert-container');
})();