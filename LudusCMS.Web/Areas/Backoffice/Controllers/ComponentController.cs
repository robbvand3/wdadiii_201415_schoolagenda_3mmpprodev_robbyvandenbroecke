﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.Viewmodels;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ComponentController : CommonController
    {
        public ActionResult CompAmountSchoolYears()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<SchoolYear>().GetAll().Count<SchoolYear>(),
                EntityType = "Schoolyear",
                EntityTypePlural = "schoolyears"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountDepartments()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Department>().GetAll().Count<Department>(),
                EntityType = "Department",
                EntityTypePlural = "departments"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountBlogs()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Blog>().GetAll().Count<Blog>(),
                EntityType = "Blog",
                EntityTypePlural = "blogs"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountCategories()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Category>().GetAll().Count<Category>(),
                EntityType = "Category",
                EntityTypePlural = "categories"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountTags()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Tag>().GetAll().Count<Tag>(),
                EntityType = "Tag",
                EntityTypePlural = "tags"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountCourses()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Course>().GetAll().Count<Course>(),
                EntityType = "Course",
                EntityTypePlural = "courses"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountClassrooms()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Classroom>().GetAll().Count<Classroom>(),
                EntityType = "Classroom",
                EntityTypePlural = "classrooms"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountClasses()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Class>().GetAll().Count<Class>(),
                EntityType = "Class",
                EntityTypePlural = "classes"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountEvents()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Event>().GetAll().Count<Event>(),
                EntityType = "Event",
                EntityTypePlural = "events"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountEventcategories()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<EventCategory>().GetAll().Count<EventCategory>(),
                EntityType = "Eventcategory",
                EntityTypePlural = "eventcategories"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountReportcards()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Reportcard>().GetAll().Count<Reportcard>(),
                EntityType = "Reportcard",
                EntityTypePlural = "reportcards"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountSchedules()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Schedule>().GetAll().Count<Schedule>(),
                EntityType = "Schedule",
                EntityTypePlural = "schedules"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountRoles()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<ApplicationRole>().GetAll().Count<ApplicationRole>(),
                EntityType = "Role",
                EntityTypePlural = "roles"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountUsers()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<ApplicationUser>().GetAll().Count<ApplicationUser>(),
                EntityType = "User",
                EntityTypePlural = "users"
            };

            return PartialView("_AmountForEntity", model);
        }

        public ActionResult CompAmountPersons()
        {
            var model = new AmountForEntity
            {
                Amount = UnitOfWork.Repository<Person>().GetAll().Count<Person>(),
                EntityType = "Person",
                EntityTypePlural = "persons"
            };

            return PartialView("_AmountForEntity", model);
        }
    }
}