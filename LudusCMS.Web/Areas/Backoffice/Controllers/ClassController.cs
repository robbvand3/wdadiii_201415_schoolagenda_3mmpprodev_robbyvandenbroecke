﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Areas.Backoffice.Controllers;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ClassController : CommonController
    {
        // GET: Backoffice/Class
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Class>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            var department = UnitOfWork.Repository<Department>().GetAll();
            ViewBag.Departments = department;

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Details(Int16? id)
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.ClassId == id &&  x.Roles.Select(y => y.RoleId).Contains(1) );

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartialDetails", model);
            }

            return View(model);
        }

        public ActionResult DeleteClass(StudentViewModel model, int? id)
        {
            //      try 
            //        {
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();
            var identiteit = originalModel.ClassId;
            originalModel.ClassId = null;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("Details", new { id = identiteit });
            //          }
            //  catch (Exception ex)
            //{
            //  var viewModel = new StudentViewModel
            //{
            //  CurrentStudent = model.CurrentStudent,
            //Classes = UnitOfWork.Repository<Class>().GetAll()
            //};

            //return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "student", model.CurrentStudent.UserName, ex));

            //            }
        }


        // GET: Backoffice/Class/Create
        public ActionResult Create()
        {
            var model = new ClassViewModel
            {
                Class = new Class(),
                Classes = UnitOfWork.Repository<Class>().GetAll(),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };

            return View(model);
        }

        // POST: Backoffice/Class/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ClassViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Class is not valid!");

                var classroom = model.Class;

                UnitOfWork.Repository<Class>().Insert(classroom);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new ClassViewModel
                {
                    Class = model.Class,
                    Classes = UnitOfWork.Repository<Class>().GetAll()
                };

                return View(viewModel);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Class By Id (primary key)
            var model = UnitOfWork.Repository<Class>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = new ClassViewModel
            {
                Class = model,
                Classes = UnitOfWork.Repository<Class>().GetBy(filter: x => x.Id != id),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ClassViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Class is not valid!");

                var originalModel = UnitOfWork.Repository<Class>().GetById(model.Class.Id);

                if (originalModel == null)
                    throw new Exception("Class do not exist!");

                originalModel.Name = model.Class.Name;
                originalModel.Description = model.Class.Description;
                originalModel.DepartmentId = model.Class.DepartmentId;


                UnitOfWork.Repository<Class>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new ClassViewModel
                {
                    Class = model.Class,
                    Classes = UnitOfWork.Repository<Class>().GetBy(filter: x => x.Id != model.Class.Id)
                };

                return View(viewModel);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Class>().GetById(id);

                if (model == null)
                    throw new Exception("Class does not exist!");

                UnitOfWork.Repository<Class>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "class", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "class", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Class>().GetById(id);

                if (model == null)
                    throw new Exception("Class does not exist!");

                UnitOfWork.Repository<Class>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "class", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "class", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Class>().GetById(id);

                if (model == null)
                    throw new Exception("Class does not exist!");

                UnitOfWork.Repository<Class>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "class", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "class", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Class");
                }
            }
        }
    }
}