﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ScheduleController : CommonController
    {
        // GET: Backoffice/Schedule
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Schedule>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Columns(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Post By Id (primary key)
            var schedule = UnitOfWork.Repository<Schedule>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Schedule, object>>> { z => z.ScheduleColumns }).First();
            
            var classes = UnitOfWork.Repository<Class>().GetAll();
           

            if (schedule == null)
            {
                return RedirectToAction("Index");
            }


            var model = new ScheduleViewModel
            {
                Schedule = schedule,
               ScheduleColumn = schedule.ScheduleColumns,
                
                Classes = classes,
                
            };

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Post By Id (primary key)
            var schedulecolumn = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<ScheduleColumn, object>>> { y => y.Class, z => z.ScheduleRows }).First();
            var scheduleColumnClass = UnitOfWork.Repository<Class>().GetById(schedulecolumn.ClassId);
            var classes = UnitOfWork.Repository<Class>().GetAll();
            var classrooms = UnitOfWork.Repository<Classroom>().GetAll();
            var courses = UnitOfWork.Repository<Course>().GetAll();

            if (schedulecolumn == null)
            {
                return RedirectToAction("Index");
            }


            var model = new ScheduleColumnViewModel
            {
                ScheduleColumn = schedulecolumn,
                Class = scheduleColumnClass,
                ScheduleRow = schedulecolumn.ScheduleRows,
                Classes = classes,
                Classrooms = classrooms,
                Courses = courses
            };

            return View(model);
        }


        

        // GET: Backoffice/AcademicYear/Create
        public ActionResult Create()
        {
            var model = new ScheduleViewModel
            {
                Schedule = new Schedule()
                
            };


            return View(model);
        }

        // POST: Backoffice/AcademicYear/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ScheduleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Schedule is not valid!");

                var schedule = model.Schedule;

                UnitOfWork.Repository<Schedule>().Insert(schedule);
                int result = UnitOfWork.SaveChanges();


                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "schedule", model.Schedule.Id));
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "schedule", ex);

                var viewModel = new ScheduleViewModel
                {
                    Schedule = model.Schedule,
                };
                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "schedule", viewModel.Schedule.Id, ex));
            }
        }



        public ActionResult PVGetScheduleRowByPostition(int position, int id)
        {


            var model = UnitOfWork.Repository<ScheduleRow>().GetBy(
                filter: f => f.Position == position && f.ScheduleColumnId == id,
               orderBy: m => m.OrderBy(p => p.Id)
                );

            var classrooms = UnitOfWork.Repository<Classroom>().GetAll();
            var courses = UnitOfWork.Repository<Course>().GetAll();
            var teachers = UnitOfWork.Repository<ApplicationUser>().GetAll();

            ViewBag.Classrooms = classrooms;
            ViewBag.Courses = courses;
            ViewBag.Teachers = teachers;

            return PartialView("_ListScheduleRow", model);
        }


        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Classroom By Id (primary key)
            var model = UnitOfWork.Repository<Schedule>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = new ScheduleViewModel
            {
                Schedule = model
           
                
            };

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ScheduleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Schedule is not valid!");

                var originalModel = UnitOfWork.Repository<Schedule>().GetById(model.Schedule.Id);

                if (originalModel == null)
                    throw new Exception("Schedule does not exist!");

                originalModel.Name = model.Schedule.Name;
                



                UnitOfWork.Repository<Schedule>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new ScheduleViewModel
                {
                    Schedule = model.Schedule
                   
                };

                return View(viewModel);
            }
        }




        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Schedule>().GetById(id);

                if (model == null)
                    throw new Exception("Schedule does not exist!");

                UnitOfWork.Repository<Schedule>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "schedule").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Schedule>().GetById(id);

                if (model == null)
                    throw new Exception("Schedule does not exist!");

                UnitOfWork.Repository<Schedule>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Schedule>().GetById(id);

                if (model == null)
                    throw new Exception("Schedule does not exist!");

                UnitOfWork.Repository<Schedule>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }



    }
}