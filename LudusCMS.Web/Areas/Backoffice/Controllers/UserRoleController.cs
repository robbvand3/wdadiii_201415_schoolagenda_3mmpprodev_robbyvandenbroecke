﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.Viewmodels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class UserRoleController : CommonController
    {
        // GET: Backoffice/User
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ApplicationUserRole>().GetAll();
            model = model.OrderByDescending(m => m.UserId);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult AddRole()
        {


            var model = new UserRoleViewModel
            {
                userrole = new ApplicationUserRole(),
                users = UnitOfWork.Repository<ApplicationUser>().GetAll(),
                roles = UnitOfWork.Repository<ApplicationRole>().GetAll()

            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddRole(UserRoleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("userrole is not valid!");


                var department = model.userrole;

                UnitOfWork.Repository<ApplicationUserRole>().Insert(department);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                var viewModel = new UserRoleViewModel
                {
                    userrole = model.userrole,

                    roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                    users = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };
                return View(viewModel);
            }
        }

        public ActionResult DeleteRole()
        {

            var model = new UserRoleViewModel
            {

                userrole = new ApplicationUserRole(),
                users = UnitOfWork.Repository<ApplicationUser>().GetAll(),
                roles = UnitOfWork.Repository<ApplicationRole>().GetAll()
                //role = UnitOfWork.Repository<ApplicationRole>().GetBy(x => x.Users.Select(y => y.UserId).Equals(id))

            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteRole(UserRoleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("userrole is not valid!");

                var department = model.userrole;

                UnitOfWork.Repository<ApplicationUserRole>().Delete(department);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                var viewModel = new UserRoleViewModel
                {
                    userrole = model.userrole,

                    roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                    users = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };
                return View(viewModel);
            }
        }
    }
}