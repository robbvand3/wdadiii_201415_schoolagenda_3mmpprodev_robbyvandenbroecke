﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class StudentController : CommonController
    {
        // GET: Backoffice/Student
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(1));
            var classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Students = model;
            ViewBag.Classes = classes;

            return View();
        }

        public ActionResult AddClass(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            var viewModel = new StudentViewModel
            {
                CurrentStudent = model,
                Classes = UnitOfWork.Repository<Class>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddClass(StudentViewModel model, int? id)
        {
      //      try 
    //        {
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

                originalModel.ClassId = model.CurrentStudent.ClassId;

                UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Edit, "student", model.CurrentStudent.UserName));
  //          }
          //  catch (Exception ex)
            //{
              //  var viewModel = new StudentViewModel
                //{
                  //  CurrentStudent = model.CurrentStudent,
                    //Classes = UnitOfWork.Repository<Class>().GetAll()
                //};

                //return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "student", model.CurrentStudent.UserName, ex));

//            }
        }

    }
}