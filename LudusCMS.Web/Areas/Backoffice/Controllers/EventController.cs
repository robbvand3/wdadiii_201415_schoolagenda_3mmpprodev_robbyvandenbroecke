﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using System.Net;
using LudusCMS.Web.Infrastructure.Alerts;


namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class EventController : CommonController
    {
        // GET: Backoffice/Event
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Event>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            var departments = UnitOfWork.Repository<Department>().GetAll();
            var courses = UnitOfWork.Repository<Course>().GetAll();
            var classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Departments = departments;
            ViewBag.Courses = courses;
            ViewBag.Classes = classes;

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Event/Create




        public ActionResult LoadCourses(Int32 huccode)
        {
           
            var courses = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == huccode);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadClasses(Int32 huccode)
        {
          
            var classes = UnitOfWork.Repository<Class>().GetBy(filter: x => x.DepartmentId == huccode);
            return Json(classes, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Create()
        {
           

            var model = new EventViewModel
            {
                Event = new Event(),
                EventCategories = UnitOfWork.Repository<EventCategory>().GetAll(),
                Courses = UnitOfWork.Repository<Course>().GetAll(),
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                
                 
                
                Classes = UnitOfWork.Repository<Class>().GetAll()

            };
            
           

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(EventViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Event is not valid!");

                var evenement = model.Event;

                UnitOfWork.Repository<Event>().Insert(evenement);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "event", ex);

                var viewModel = new EventViewModel
                {
                    Event = model.Event,
                    EventCategories = UnitOfWork.Repository<EventCategory>().GetAll(),
                    Courses = UnitOfWork.Repository<Course>().GetAll(),
                    Departments = UnitOfWork.Repository<Department>().GetAll(),
                Classes = UnitOfWork.Repository<Class>().GetAll()
                };
                return View(viewModel).WithError(msg);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");           
            }

            var model = new EventViewModel
            {
                Event = UnitOfWork.Repository<Event>().GetById(id),
                EventCategories = UnitOfWork.Repository<EventCategory>().GetAll(),
                Courses = UnitOfWork.Repository<Course>().GetAll(),
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                Classes = UnitOfWork.Repository<Class>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(EventViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Event is not valid!");

                var originalModel = UnitOfWork.Repository<Event>().GetById(model.Event.Id);

                if (originalModel == null)
                    throw new Exception("Event does not exist!");

                originalModel.Name = model.Event.Name;
                originalModel.Description = model.Event.Description;
                originalModel.Start_Date = model.Event.Start_Date;
                originalModel.End_Date = model.Event.End_Date;
                originalModel.EventCategoryId = model.Event.EventCategoryId;
                originalModel.CourseId = model.Event.CourseId;
                originalModel.Course = model.Event.Course;
                originalModel.DepartmentId = model.Event.DepartmentId;
                originalModel.ClassId = model.Event.ClassId;
      
                UnitOfWork.Repository<Event>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Event>().GetById(id);

                if (model == null)
                    throw new Exception("Event does not exist!");

                UnitOfWork.Repository<Event>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "event", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "event", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Event>().GetById(id);

                if (model == null)
                    throw new Exception("Event does not exist!");

                UnitOfWork.Repository<Event>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "event", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "event", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Event>().GetById(id);

                if (model == null)
                    throw new Exception("Event does not exist!");

                UnitOfWork.Repository<Event>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "event", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "event", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Event").WithError(msg);
                }
            }
        }

    }
}