﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class EventcategoryController : CommonController
    {
        // GET: Backoffice/Eventcategory
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<EventCategory>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Eventcategory/Create
        public ActionResult Create()
        {
            var model = new EventCategory();//Create empty model

            return View(model);
        }

        // POST: Backoffice/Eventcategory/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(EventCategory model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Eventcategory is not valid!");


                UnitOfWork.Repository<EventCategory>().Insert(model);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Course By Id (primary key)
            var model = UnitOfWork.Repository<EventCategory>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(Course model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Eventcategory is not valid!");

                var originalModel = UnitOfWork.Repository<EventCategory>().GetById(model.Id);

                if (originalModel == null)
                    throw new Exception("Eventcategory do not exist!");

                originalModel.Name = model.Name;


                UnitOfWork.Repository<EventCategory>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<EventCategory>().GetById(id);

                if (model == null)
                    throw new Exception("EventCategory does not exist!");

                UnitOfWork.Repository<EventCategory>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "eventcategory", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "eventcategory", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<EventCategory>().GetById(id);

                if (model == null)
                    throw new Exception("EventCategory does not exist!");

                UnitOfWork.Repository<EventCategory>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "eventcategory", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "eventcategory", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<EventCategory>().GetById(id);

                if (model == null)
                    throw new Exception("EventCategory does not exist!");

                UnitOfWork.Repository<EventCategory>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "eventcategory", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "eventcategory", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "EventCategory").WithError(msg);
                }
            }
        }
    }
}