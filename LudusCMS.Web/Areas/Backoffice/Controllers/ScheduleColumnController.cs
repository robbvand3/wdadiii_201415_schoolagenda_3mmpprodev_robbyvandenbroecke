﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ScheduleColumnController : CommonController
    {
        // GET: Backoffice/Reportcard
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ScheduleColumn>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<ScheduleColumn, object>>> { z => z.Class });
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }



        



        public ActionResult Create(Int16 id)
        {
            var model = new ScheduleColumnViewModel
            { 
                ScheduleColumn = new ScheduleColumn(),
                Classes = UnitOfWork.Repository<Class>().GetAll(),
                Schedule = UnitOfWork.Repository<Schedule>().GetById(id)


            };


            return View(model);
        }

        // POST: Backoffice/Reportcard/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ScheduleColumnViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("ScheduleColumn is not valid!");

                var schedulecolumn = model.ScheduleColumn;
                var scheduleid = model.ScheduleColumn.ScheduleId;

                UnitOfWork.Repository<ScheduleColumn>().Insert(schedulecolumn);
                int result = UnitOfWork.SaveChanges();


                return RedirectToAction("Columns", "Schedule", new { id = scheduleid }).WithSuccess(CreateMessage(ControllerActionType.Create, "schedulecolumn", model.ScheduleColumn.Id));
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "schedulecolumn", ex);

                var viewModel = new ScheduleColumnViewModel
                {
                    ScheduleColumn = model.ScheduleColumn,
                    Schedule = model.Schedule,
                    Classes = UnitOfWork.Repository<Class>().GetAll(),
                };
                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "schedulecolumn", viewModel.ScheduleColumn.Id, ex));
            }
        }
        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Reportcard By Id (primary key)
            var model = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<ScheduleColumn, object>>> { x => x.ScheduleRows }).First();

            var viewmodel = new ScheduleColumnViewModel
            {
                ScheduleColumn = model,
                ScheduleRow = model.ScheduleRows,
                Classes = UnitOfWork.Repository<Class>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewmodel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ScheduleColumnViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Reportcard is not valid!");

                var originalModel = UnitOfWork.Repository<ScheduleColumn>().GetById(model.ScheduleColumn.Id);

                if (originalModel == null)
                    throw new Exception("Reportcard do not exist!");


                
                originalModel.ClassId = model.ScheduleColumn.ClassId;
                originalModel.ScheduleRows = model.ScheduleColumn.ScheduleRows;
                originalModel.ScheduleId = model.ScheduleColumn.ScheduleId;

                UnitOfWork.Repository<ScheduleColumn>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Columns", "Schedule", new {id =model.ScheduleColumn.ScheduleId }).WithSuccess(CreateMessage(ControllerActionType.Edit, "schedule", model.ScheduleColumn.Id));
            }
            catch (Exception ex)
            {
                var viewmodel = new ScheduleColumnViewModel
                {
                    Classes = UnitOfWork.Repository<Class>().GetAll()
                };

                return View(viewmodel).WithError(CreateMessage(ControllerActionType.Create, "schedule", model.ScheduleColumn.Id, ex));
            }
        }
     
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleColumn>().GetById(id);
                var identiteit = model.ScheduleId;
                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<ScheduleColumn>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Columns", "Schedule", new { id = model.ScheduleId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {

                var msg = CreateMessage(ControllerActionType.Delete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleColumn>().GetById(id);

                if (model == null)
                    throw new Exception("Schedule does not exist!");

                UnitOfWork.Repository<ScheduleColumn>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleColumn>().GetById(id);

                if (model == null)
                    throw new Exception("Schedule does not exist!");

                UnitOfWork.Repository<ScheduleColumn>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Schedule").WithError(msg);
                }
            }
        }
    }
}