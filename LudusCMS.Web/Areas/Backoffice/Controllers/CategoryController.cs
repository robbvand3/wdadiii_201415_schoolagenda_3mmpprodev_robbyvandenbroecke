﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Areas.Backoffice.Controllers;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class CategoryController : CommonController
    {
        // GET: Backoffice/Category
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Category>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Category/Create
        public ActionResult Create()
        {
            var model = new CategoryViewModel
            {
                Category = new Category(),
                Categories = UnitOfWork.Repository<Category>().GetAll()
            };

            return View(model);
        }

        // POST: Backoffice/Category/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CategoryViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Category is not valid!");

                var category = model.Category;

                UnitOfWork.Repository<Category>().Insert(category);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new CategoryViewModel
                {
                    Category = model.Category,
                    Categories = UnitOfWork.Repository<Category>().GetAll()
                };

                return View(viewModel);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Category By Id (primary key)
            var model = UnitOfWork.Repository<Category>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = new CategoryViewModel
            {
                Category = model,
                Categories = UnitOfWork.Repository<Category>().GetBy(filter: x => x.Id != id)
            };

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Category is not valid!");

                var originalModel = UnitOfWork.Repository<Category>().GetById(model.Category.Id);

                if (originalModel == null)
                    throw new Exception("Category do not exist!");

                originalModel.Name = model.Category.Name;
                originalModel.Description = model.Category.Description;
                

                UnitOfWork.Repository<Category>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new CategoryViewModel
                {
                    Category = model.Category,
                    Categories = UnitOfWork.Repository<Category>().GetBy(filter: x => x.Id != model.Category.Id)
                };

                return View(viewModel);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Category>().GetById(id);

                if (model == null)
                    throw new Exception("Category does not exist!");

                UnitOfWork.Repository<Category>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "category", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "category", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Category>().GetById(id);

                if (model == null)
                    throw new Exception("Category does not exist!");

                UnitOfWork.Repository<Category>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "category", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "category", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Category>().GetById(id);

                if (model == null)
                    throw new Exception("Category does not exist!");

                UnitOfWork.Repository<Category>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "category", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "category", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Category");
                }
            }
        }
    }
}