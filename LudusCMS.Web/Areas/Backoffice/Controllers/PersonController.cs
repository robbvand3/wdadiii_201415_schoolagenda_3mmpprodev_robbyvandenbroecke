﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using System.Threading.Tasks;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using LudusCMS.Model.Viewmodels;
using Microsoft.AspNet.Identity;


namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class PersonController : CommonController
    {
        // GET: Backoffice/Person
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Person>().GetAll();
            var departments = UnitOfWork.Repository<Department>().GetAll();
            var roles = UnitOfWork.Repository<ApplicationRole>().GetAll();

            ViewBag.Persons = model;
            ViewBag.departments = departments;
            ViewBag.roles = roles;

            return View();
        }
         // GET: Backoffice/Person/Create
        public ActionResult Create()
        {
            var model = new PersonViewModel
            {
                Person = new Person(),
                Roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };
            return View(model);
        }
        // POST: Backoffice/AcademicYear/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(PersonViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Person is not valid!");

                var person = model.Person;

                UnitOfWork.Repository<Person>().Insert(person);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new PersonViewModel
                {
                    Person = model.Person,
                    Roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                    Departments = UnitOfWork.Repository<Department>().GetAll()
                };
                return View(viewModel);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = new PersonViewModel
            {
                Person = UnitOfWork.Repository<Person>().GetById(id),
                Roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                Departments = UnitOfWork.Repository<Department>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(PersonViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Person is not valid!");

                var originalModel = UnitOfWork.Repository<Person>().GetById(model.Person.Id);

                if (originalModel == null)
                    throw new Exception("Person does not exist!");

                originalModel.FirstName = model.Person.FirstName;
                originalModel.LastName = model.Person.LastName;
                originalModel.Email = model.Person.Email;
                originalModel.DayOfBirth = model.Person.DayOfBirth;
                
                originalModel.DepartmentId = model.Person.DepartmentId;


                //originalModel.AcademicYear = model.Department.AcademicYear;

                UnitOfWork.Repository<Person>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Person>().GetById(id);

                if (model == null)
                    throw new Exception("Person does not exist!");

                UnitOfWork.Repository<Person>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "person", model.FirstName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "person", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Person>().GetById(id);

                if (model == null)
                    throw new Exception("Person does not exist!");

                UnitOfWork.Repository<Person>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schoolyear", model.FirstName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "person", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Person>().GetById(id);

                if (model == null)
                    throw new Exception("Person does not exist!");

                UnitOfWork.Repository<Person>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schoolyear", model.FirstName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "person", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Person").WithError(msg);
                }
            }
        }

        
        public ActionResult MakeUser(Int16? id)
        {
            var orignalmodel = UnitOfWork.Repository<Person>().GetById(id);
            var model = new UserRegisterViewModel
            {

                FirstName = orignalmodel.FirstName,
                LastName = orignalmodel.LastName,
                Email = orignalmodel.Email,
                DepartmentId = orignalmodel.DepartmentId,
                Departments = UnitOfWork.Repository<Department>().GetAll()




            };
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> MakeUser(UserRegisterViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("User is not valid");

                var originalModel = UnitOfWork.Repository<Person>().GetById(model.Id);

                //Create a new ApplicationUser
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, DepartmentId = model.DepartmentId, };

                //Create the user via the UserManager
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "user", model.UserName));
                }
                else
                {
                    //AddErrors(result);
                    throw new Exception("Can't create the user!");
                }

            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "user", model.UserName, ex));
            }
        }
    }

}