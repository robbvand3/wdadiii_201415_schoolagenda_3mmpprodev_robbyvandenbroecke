﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class BlogController : CommonController
    {
        // GET: Backoffice/Blog
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Blog>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Blog, object>>> { x => x.Tags, y => y.Categories, z => z.Department });
            model = model.OrderByDescending(m => m.Id);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Post/Create
        public ActionResult Create()
        {
            var filteredTags = UnitOfWork.Repository<Tag>().GetAll();
            var filteredCategories = UnitOfWork.Repository<Category>().GetAll();
            
            var model = new BlogViewModel
            {
                Blog = new Blog(),
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                Tags = new MultiSelectList(filteredTags, "Id", "Name"),
                Categories = new MultiSelectList(filteredCategories, "Id", "Name")
            };
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(BlogViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Blog is not valid!");

                var blog = model.Blog;
                var ids = model.SelectedTagIds;
                var catids = model.SelectedCategoryIds;

                if (ids != null && ids.Length > 0)
                {
                    var tags = new List<Tag>();
                    foreach (var id in ids)
                    {
                        var tag = UnitOfWork.Repository<Tag>().GetById(id);
                        tags.Add(tag);
                    }
                    blog.Tags = tags;
                }

                if (catids != null && catids.Length > 0)
                {
                    var cats = new List<Category>();
                    foreach (var catid in catids)
                    {
                        var cat = UnitOfWork.Repository<Category>().GetById(catid);
                        cats.Add(cat);
                    }
                    blog.Categories = cats;
                }

                UnitOfWork.Repository<Blog>().Insert(blog);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "blog", model.Blog.Title));
            }
            catch (Exception ex)
            {
                var filteredCategories = UnitOfWork.Repository<Category>().GetAll();
                var filteredTags = UnitOfWork.Repository<Tag>().GetAll();

                var viewModel = new BlogViewModel
                {
                    Blog = new Blog(),
                    Departments = UnitOfWork.Repository<Department>().GetAll(),
                    Tags = new MultiSelectList(filteredTags, "Id", "Name"),
                    Categories = new MultiSelectList(filteredCategories, "Id", "Name")
                };

                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "blog", model.Blog.Title, ex));
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var blog = UnitOfWork.Repository<Blog>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Blog, object>>> { x => x.Tags, y => y.Categories }).First();
            if (blog == null)
                throw new Exception("Blog does not exist!");

            var filteredCategories = UnitOfWork.Repository<Category>().GetAll();
            var filteredTags = UnitOfWork.Repository<Tag>().GetAll();

            int[] ids = null;
            if (blog.Tags != null && blog.Tags.Count > 0)
            {
                ids = new int[blog.Tags.Count];
                int i = 0;
                foreach (var tag in blog.Tags)
                {
                    ids[i] = tag.Id;
                    i++;
                }
            }

            int[] catids = null;
            if (blog.Categories != null && blog.Categories.Count > 0)
            {
                catids = new int[blog.Categories.Count];
                int y = 0;
                foreach (var cat in blog.Categories)
                {
                    catids[y] = cat.Id;
                    y++;
                }
            }

            var model = new BlogViewModel
            {
                Blog = blog,
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                Tags = new MultiSelectList(filteredTags, "Id", "Name"),
                Categories = new MultiSelectList(filteredCategories, "Id", "Name"),
                SelectedTagIds = ids,
                SelectedCategoryIds = catids
            };

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(BlogViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Blog is not valid!");

                var originalModel = UnitOfWork.Repository<Blog>().GetBy(filter: x => x.Id == (Int32)model.Blog.Id, includes: new List<System.Linq.Expressions.Expression<Func<Blog, object>>> { x => x.Tags }).First();

                if (originalModel == null)
                    throw new Exception("Blog does not exist!");

                originalModel.Title = model.Blog.Title;
                originalModel.Synopsis = model.Blog.Synopsis;
                originalModel.Content = model.Blog.Content;

                Department department = null;
                if (model.Blog.DepartmentId != null)
                {
                    department = UnitOfWork.Repository<Department>().GetById(model.Blog.DepartmentId);
                }
                originalModel.DepartmentId = model.Blog.DepartmentId;

                var ids = model.SelectedTagIds;
                if (ids != null && ids.Length > 0)
                {
                    if (originalModel.Tags != null)
                        originalModel.Tags.Clear();

                    var tags = new List<Tag>();
                    foreach (var id in ids)
                    {
                        var tag = UnitOfWork.Repository<Tag>().GetById(id);
                        tags.Add(tag);
                    }
                    originalModel.Tags = tags;
                }

                var catids = model.SelectedCategoryIds;
                if (catids != null && catids.Length > 0)
                {
                    if (originalModel.Categories != null)
                        originalModel.Categories.Clear();

                    var cats = new List<Category>();
                    foreach (var catid in catids)
                    {
                        var cat = UnitOfWork.Repository<Category>().GetById(catid);
                        cats.Add(cat);
                    }
                    originalModel.Categories = cats;
                }

                UnitOfWork.Repository<Blog>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Edit, "blog", model.Blog.Title));
            }
            catch (Exception ex)
            {
                var filteredTags = UnitOfWork.Repository<Tag>().GetAll();
                var filteredCategories = UnitOfWork.Repository<Category>().GetAll();

                var viewModel = new BlogViewModel
                {
                    Blog = model.Blog,
                    Departments = UnitOfWork.Repository<Department>().GetAll(),
                    Tags = new MultiSelectList(filteredTags, "Id", "Name"),
                    Categories = new MultiSelectList(filteredCategories, "Id", "Name"),
                    SelectedTagIds = model.SelectedTagIds,
                    SelectedCategoryIds = model.SelectedCategoryIds
                };

                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "blog", model.Blog.Title, ex));
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Blog>().GetById(id);

                if (model == null)
                    throw new Exception("Blog does not exist!");

                UnitOfWork.Repository<Blog>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "blog", model.Title);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "blog", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Blog>().GetById(id);

                if (model == null)
                    throw new Exception("Post does not exist!");

                UnitOfWork.Repository<Blog>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "blog", model.Title);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "blog", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Blog>().GetById(id);

                if (model == null)
                    throw new Exception("Blog does not exist!");

                UnitOfWork.Repository<Blog>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "blog", model.Title);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "blog", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Blog").WithError(msg);
                }
            }
        }

       
    }
    


}