﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.Viewmodels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class RoleController : CommonController
    {
        // GET: Backoffice/User
        public ActionResult Index()
        {
            var model = RoleManager.Roles;
            model = model.OrderByDescending(m => m.Name);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new RoleViewModel();

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Role is not valid");

                var role = new ApplicationRole { Name = model.Name, Description = model.Description };
                var result = await RoleManager.CreateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "role", model.Name));
                }
                else
                {
                    AddErrors(result);
                    throw new Exception("Can't create the role!");
                }

            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "role", model.Name, ex));
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var original = UnitOfWork.Repository<ApplicationRole>().GetById(id);
            var model = new RoleViewModel()
            {
               
               // role = UnitOfWork.Repository<ApplicationRole>().GetById(id),

                Description = original.Description,
                Name = original.Name


               // Name = UnitOfWork.Repository<ApplicationRole>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            
            try
            {
                
                if (!ModelState.IsValid)
                    throw new Exception("Role is not valid!");
                
                var originalModel = UnitOfWork.Repository<RoleViewModel>().GetById(model.role.Id);

                if (originalModel == null)
                    throw new Exception("Event does not exist!");

                originalModel.Name = model.Name;
                originalModel.Description = model.Name;
                originalModel.role = model.role;
               
                UnitOfWork.Repository<RoleViewModel>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var model = await RoleManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("Role does not exist!");

                var result = await RoleManager.DeleteAsync(model);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.Delete, "role", model.Name);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't delete the role!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "role", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> VirtualDelete(int id)
        {
            try
            {
                var model = await RoleManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("Role does not exist!");

                model.Deleted = DateTime.Now;
                model.Updated = DateTime.Now;
                var result = await RoleManager.UpdateAsync(model);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.VirtualDelete, "role", model.Name);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't soft-delete the role!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "role", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> VirtualUnDelete(int id)
        {
            try
            {
                var model = await RoleManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("Role does not exist!");

                model.Deleted = null;
                model.Updated = DateTime.Now;
                var result = await RoleManager.UpdateAsync(model);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "role", model.Name);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't soft-undelete the role!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "role", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Role").WithError(msg);
                }
            }
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}