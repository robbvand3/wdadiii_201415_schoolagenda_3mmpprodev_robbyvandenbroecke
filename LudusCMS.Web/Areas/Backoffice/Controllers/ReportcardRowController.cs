﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ReportcardRowController : CommonController
    {
              // GET: Backoffice/Reportcard/Create
        public ActionResult Create(int? id)
        {
                if (id == null)
                {
                    return RedirectToAction("Index", "Reportcard");
                }

            var reportcard = UnitOfWork.Repository<Reportcard>().GetById(id);

            var user = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == reportcard.UserId).First();
            var department = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == user.DepartmentId).First();

            var model = new ReportcardRowViewModel
            {
                ReportcardRow = new ReportcardRow(),
                Reportcard = UnitOfWork.Repository<Reportcard>().GetById(id),
                Courses = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == department.Id)
            };

            return View(model);
        }

        // POST: Backoffice/Reportcard/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ReportcardRowViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Reportcardrow is not valid!");

                var reportcardrow = model.ReportcardRow;
                var reportId = model.ReportcardRow.ReportcardId;

                reportcardrow.TaskScore= (reportcardrow.TaskPoint / reportcardrow.TaskTotal) * 100;
                reportcardrow.TestScore = (reportcardrow.TestPoint / reportcardrow.TestTotal) * 100;
                reportcardrow.TotalScore = (reportcardrow.TestScore + reportcardrow.TaskScore) / 2;
                UnitOfWork.Repository<ReportcardRow>().Insert(reportcardrow);
                int result = UnitOfWork.SaveChanges();


                return RedirectToAction("Details", "Reportcard", new { id = reportId }).WithSuccess(CreateMessage(ControllerActionType.Create, "reportcardrow", model.ReportcardRow.Id));
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "reportcardrow", ex);



                var viewModel = new ReportcardRowViewModel
                {
                    ReportcardRow = model.ReportcardRow,
                    Reportcard = model.Reportcard,
                    Courses = UnitOfWork.Repository<Course>().GetAll()
                };
                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "reportcardrw", viewModel.ReportcardRow.Id, ex));
            }
        }
        public ActionResult Edit(Int16? id)
        {
           

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = new ReportcardRowViewModel()
            {
                ReportcardRow = UnitOfWork.Repository<ReportcardRow>().GetById(id),
                Courses = UnitOfWork.Repository<Course>().GetAll(),


                
            };

            if (model == null)
            {
                return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardRow.ReportcardId }).WithSuccess(CreateMessage(ControllerActionType.Create, "reportcardrow", model.Reportcard.Id));
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ReportcardRowViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("ReportcardRow is not valid!");

                var originalModel = UnitOfWork.Repository<ReportcardRow>().GetById(model.ReportcardRow.Id);

                if (originalModel == null)
                    throw new Exception("Reportcard do not exist!");

                originalModel.CourseId = model.ReportcardRow.CourseId;
                originalModel.Description = model.ReportcardRow.Description;
                originalModel.TaskPoint = model.ReportcardRow.TaskPoint;
                originalModel.TestPoint = model.ReportcardRow.TestPoint;
                originalModel.TaskTotal = model.ReportcardRow.TaskTotal;
                originalModel.TestTotal = model.ReportcardRow.TestTotal;
               
                originalModel.ReportcardId = model.ReportcardRow.ReportcardId;
                originalModel.TaskScore = (model.ReportcardRow.TaskPoint / model.ReportcardRow.TaskTotal) * 100;
                originalModel.TestScore = (model.ReportcardRow.TestPoint / model.ReportcardRow.TestTotal) * 100;
                originalModel.TotalScore = (originalModel.TestScore + originalModel.TaskScore) / 2;


                UnitOfWork.Repository<ReportcardRow>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardRow.ReportcardId }).WithSuccess(CreateMessage(ControllerActionType.Edit, "schoolyear", model.ReportcardRow.Id));
            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "reportcardrow", model.ReportcardRow.Id, ex));
            }
        }

        public ActionResult Delete(int id, int reportcardId)
        {
            try
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);
                
                if (model == null)
                    throw new Exception("Reportcardrow does not exist!");

                UnitOfWork.Repository<ReportcardRow>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "reportcardrow", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = reportcardId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.Delete, "reportcardrow", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardId }).WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);

                if (model == null)
                    throw new Exception("ReportcardRow does not exist!");

                UnitOfWork.Repository<ReportcardRow>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "reportcardrow", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "reportcard", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardId }).WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcardrow does not exist!");

                UnitOfWork.Repository<ReportcardRow>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "reportcard", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "reportcard", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Reportcard", new { id = model.ReportcardId }).WithError(msg);
                }
            }
        }
    }
}