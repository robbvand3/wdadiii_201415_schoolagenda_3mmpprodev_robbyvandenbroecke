﻿using Data.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Data.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public enum ControllerActionType
    {
        Index = 0,
        Create = 1,
        Edit = 2,
        Delete = 3,
        VirtualDelete = 4,
        VirtualUnDelete = 5,
        Lock = 6,
        UnLock = 7,
        EnableTwoFactor = 8,
        DisableTwoFactor = 9
    }

    public class CommonController : Controller
    {
        #region Variables
        private GenericUnitOfWork _unitOfWork;
        private ApplicationUserManager _applicationUserManager;
        private ApplicationRoleManager _applicationRoleManager;
        private string MSGCREATEOK = "Created the {0} {1} in the database! {2}";
        private string MSGCREATENOK = "Could not create the {0} {1} in the database! {2}";
        private string MSGEDITOK = "Updated the {0} {1} in the database! {2}";
        private string MSGEDITNOK = "Could not update the {0} {1} in the database! {2}";
        private string MSGDELETEOK = "Deleted the {0} {1} in the database! {2}";
        private string MSGDELETENOK = "Could not delete the {0} {1} in the database! {2}";
        private string MSGVIRTUALDELETEOK = "Soft-deleted the {0} {1} in the database! {2}";
        private string MSGVIRTUALDELETENOK = "Could not soft-delete the {0} {1} in the database! {2}";
        private string MSGVIRTUALUNDELETEOK = "Soft-undeleted the {0} {1} in the database! {2}";
        private string MSGVIRTUALUNDELETENOK = "Could not soft-undelete the {0} {1} in the database! {2}";
        private string MSGENABLETWOFACTOROK = "Enabled Two Factor for the {0} {1} in the database! {2}";
        private string MSGENABLETWOFACTORNOK = "Could not enable Two Factor for the {0} {1} in the database! {2}";
        private string MSGDISABLETWOFACTOROK = "Disabled Two Factor for the {0} {1} in the database! {2}";
        private string MSGDISABLETWOFACTORNOK = "Could not disable Two Factor for the {0} {1} in the database! {2}";
        #endregion

        #region Properties
        protected GenericUnitOfWork UnitOfWork
        {
            get
            {
                if (_unitOfWork == null)
                {
                    _unitOfWork = new GenericUnitOfWork(new ApplicationDbContext());
                }
                return _unitOfWork;
            }
        }
       public ApplicationUserManager UserManager
       {
            get
            {
                return _applicationUserManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();
            }
            private set
            {
               _applicationUserManager = value;
            }
       }

       public ApplicationRoleManager RoleManager
       {
           get
            {
                return _applicationRoleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _applicationRoleManager = value;
            }
        }
        #endregion

        #region Utilities
        protected string CreateMessage(ControllerActionType actionType, string type, Object id, Exception ex = null)
        {
            var msg = "";

            switch (actionType)
            {
                case ControllerActionType.Create:
                default:
                    msg = (ex == null) ? MSGCREATEOK : MSGCREATENOK; break;
                case ControllerActionType.Edit:
                    msg = (ex == null) ? MSGEDITOK : MSGEDITNOK; break;
                case ControllerActionType.Delete:
                    msg = (ex == null) ? MSGDELETEOK : MSGDELETENOK; break;
                case ControllerActionType.VirtualDelete:
                    msg = (ex == null) ? MSGVIRTUALDELETEOK : MSGVIRTUALDELETENOK; break;
                case ControllerActionType.VirtualUnDelete:
                    msg = (ex == null) ? MSGVIRTUALUNDELETEOK : MSGVIRTUALUNDELETENOK; break;
                case ControllerActionType.EnableTwoFactor:
                    msg = (ex == null) ? MSGENABLETWOFACTOROK : MSGENABLETWOFACTORNOK; break;
                case ControllerActionType.DisableTwoFactor:
                    msg = (ex == null) ? MSGDISABLETWOFACTOROK : MSGDISABLETWOFACTORNOK; break;
            }

            msg = String.Format(msg, type, id, ((ex != null) ? ex.ToString() : ""));

            return msg;
        }
        #endregion
    }
}