﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Areas.Backoffice.Controllers;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ClassroomController : CommonController
    {
        // GET: Backoffice/Classroom
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Classroom>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/Class/Create
        public ActionResult Create()
        {
            var model = new ClassroomViewModel
            {
                Classroom = new Classroom(),
                Classrooms = UnitOfWork.Repository<Classroom>().GetAll()
            };

            return View(model);
        }

        // POST: Backoffice/Class/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ClassroomViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Class is not valid!");

                var lokaal = model.Classroom;

                UnitOfWork.Repository<Classroom>().Insert(lokaal);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new ClassroomViewModel
                {
                    Classroom = model.Classroom,
                    Classrooms = UnitOfWork.Repository<Classroom>().GetAll()
                };

                return View(viewModel);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Classroom By Id (primary key)
            var model = UnitOfWork.Repository<Classroom>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = new ClassroomViewModel
            {
                Classroom = model,
                Classrooms = UnitOfWork.Repository<Classroom>().GetBy(filter: x => x.Id != id)
            };

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ClassroomViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Class is not valid!");

                var originalModel = UnitOfWork.Repository<Classroom>().GetById(model.Classroom.Id);

                if (originalModel == null)
                    throw new Exception("Class do not exist!");

                originalModel.Name = model.Classroom.Name;
                


                UnitOfWork.Repository<Classroom>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new ClassroomViewModel
                {
                    Classroom = model.Classroom,
                    Classrooms = UnitOfWork.Repository<Classroom>().GetBy(filter: x => x.Id != model.Classroom.Id)
                };

                return View(viewModel);
            }
        }

       
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Classroom>().GetById(id);

                if (model == null)
                    throw new Exception("Classroom does not exist!");

                UnitOfWork.Repository<Classroom>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "classroom", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "classroom", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Classroom>().GetById(id);

                if (model == null)
                    throw new Exception("Classroom does not exist!");

                UnitOfWork.Repository<Classroom>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "classroom", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "classroom", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Classroom>().GetById(id);

                if (model == null)
                    throw new Exception("Classroom does not exist!");

                UnitOfWork.Repository<Classroom>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "classroom", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "classroom", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Classroom");
                }
            }
        }
    }
}