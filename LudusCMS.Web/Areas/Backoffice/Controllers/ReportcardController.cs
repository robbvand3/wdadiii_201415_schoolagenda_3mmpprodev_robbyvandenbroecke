﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ReportcardController : CommonController
    {
        // GET: Backoffice/Reportcard
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Reportcard>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Reportcard, object>>> { z => z.User });
            model = model.OrderByDescending(m => m.Created);

            var users = UnitOfWork.Repository<ApplicationUser>().GetAll();
            ViewBag.Users = users;

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Post By Id (primary key)
            var reportcard = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Reportcard, object>>> { y => y.User, z => z.ReportcardRows }).First();
            var user = UnitOfWork.Repository<ApplicationUser>().GetById(reportcard.UserId);
            var courses = UnitOfWork.Repository<Course>().GetAll();

            if (reportcard == null)
            {
                return RedirectToAction("Index");
            }


            var model = new ReportCardViewModel
            {
                Reportcard = reportcard,
                User = user,
                ReportcardRows = reportcard.ReportcardRows,
                Courses = courses
            };

         //   ViewBag.Courses = courses;
         //   ViewBag.Model = model;
            return View(model);
        }

        // GET: Backoffice/Reportcard/Create
        public ActionResult Create()
        {
            var model = new ReportCardViewModel
            {
                Reportcard = new Reportcard(),
                ApplicationUsers = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(1))
            };
            return View(model);
        }
        // POST: Backoffice/Reportcard/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ReportCardViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Reportcard is not valid!");

                var reportcard = model.Reportcard;

                UnitOfWork.Repository<Reportcard>().Insert(reportcard);
                int result = UnitOfWork.SaveChanges();


                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "reportcard", model.Reportcard.Id));
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "reportcard", ex);

                var viewModel = new ReportCardViewModel
                {
                    Reportcard = model.Reportcard,
                    ApplicationUsers = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };
                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "reportcard", viewModel.Reportcard.Id, ex));
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Reportcard By Id (primary key)
            var model = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Reportcard, object>>> { x => x.ReportcardRows, y => y.Users }).First();
            ViewBag.ApplicationUsers = UnitOfWork.Repository<ApplicationUser>().GetAll();
            var viewmodel = new ReportCardViewModel
            {
                Reportcard = model,
                ReportcardRows = model.ReportcardRows,
               
                ApplicationUsers = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(1))
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewmodel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ReportCardViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Reportcard is not valid!");

                var originalModel = UnitOfWork.Repository<Reportcard>().GetById(model.Reportcard.Id);

                if (originalModel == null)
                    throw new Exception("Reportcard does not exist!");

                originalModel.Title = model.Reportcard.Title;
                originalModel.UserId= model.Reportcard.UserId;
                originalModel.User = model.Reportcard.User;
  
                
                
               
                
                
                UnitOfWork.Repository<Reportcard>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }



        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Reportcard>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<Reportcard>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "Reportcard", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "reportcard", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Reportcard>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<Reportcard>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "reportcard", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "reportcard", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Reportcard>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<Reportcard>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "reportcard", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "reportcard", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithError(msg);
                }
            }
        }



        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteRow(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<ReportcardRow>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "schoolyear", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Reportcard").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "reportcardRow", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "ReportcardRow").WithError(msg);
                }
            }
        }




    }
}