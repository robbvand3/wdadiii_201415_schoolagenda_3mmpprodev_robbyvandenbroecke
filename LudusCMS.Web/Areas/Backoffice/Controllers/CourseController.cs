﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class CourseController : CommonController
    {
        // GET: Backoffice/Course
        public ActionResult Index()
        {
            var departments = UnitOfWork.Repository<Department>().GetAll();
            var teachers = UnitOfWork.Repository<ApplicationUser>().GetAll();
           
            ViewBag.Departments = departments;
            ViewBag.Teachers = teachers;

            var model = UnitOfWork.Repository<Course>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/course/Create
        public ActionResult Create()
        {
            var model = new CourseViewModel()
            {
                course = new Course(),
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                events = UnitOfWork.Repository<Event>().GetAll(),
                Teachers = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(2) || x.Roles.Select(y => y.RoleId).Contains(6))
            };

            return View(model);
        }

        // POST: Backoffice/course/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CourseViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Course is not valid!");

                var course = model.course;

                UnitOfWork.Repository<Course>().Insert(course);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new CourseViewModel
                {
                   course = model.course,
                    Departments = UnitOfWork.Repository<Department>().GetAll(),
                    events = UnitOfWork.Repository<Event>().GetAll(),
                    Teachers = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(2) || x.Roles.Select(y => y.RoleId).Contains(6) )
                };
                return View(viewModel);
            }
        }
        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = new CourseViewModel
            {
                course = UnitOfWork.Repository<Course>().GetById(id),
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                events = UnitOfWork.Repository<Event>().GetAll(),
                Teachers = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(2) || x.Roles.Select(y => y.RoleId).Contains(6))
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(CourseViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Course is not valid!");

                var originalModel = UnitOfWork.Repository<Course>().GetById(model.course.Id);

                if (originalModel == null)
                    throw new Exception("Department do not exist!");

                originalModel.Name = model.course.Name;
                originalModel.Description = model.course.Description;
                originalModel.DepartmentId= model.course.DepartmentId;
                originalModel.TeacherId = model.course.TeacherId;
                
           

                UnitOfWork.Repository<Course>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

       
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);

                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<Course>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "course", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "course", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete2(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);
                var departmentid = model.DepartmentId;
                if (model == null)
                    throw new Exception("Reportcard does not exist!");

                UnitOfWork.Repository<Course>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "course", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Department", new { id = departmentid  }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "course", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);

                if (model == null)
                    throw new Exception("Course does not exist!");

                UnitOfWork.Repository<Course>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "course", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "course", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);

                if (model == null)
                    throw new Exception("Course does not exist!");

                UnitOfWork.Repository<Course>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "course", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "course", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Course").WithError(msg);
                }
            }
        }
    }
}