﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.Viewmodels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class UserController : CommonController
    {
        // GET: Backoffice/User
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetAll();
            model = model.OrderByDescending(m => m.UserName);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new UserRegisterViewModel
            {
                 User = new ApplicationUser(),
                 
            };
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Create(UserRegisterViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("User is not valid");

                //Create a new ApplicationUser
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName};
               
                //Create the user via the UserManager
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "user", model.UserName));
                }
                else
                {
                    AddErrors(result);
                    throw new Exception("Can't create the user!");
                }

            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "user", model.UserName, ex));
            }
        }

        public ActionResult Edit(Int16? id)
        {
            var orignalmodel = UnitOfWork.Repository<ApplicationUser>().GetById(id);
           
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var model = new ApplicationUser()
            {
               
                
                FirstName = orignalmodel.FirstName,
                LastName= orignalmodel.LastName,
                Email = orignalmodel.Email,
                UserName = orignalmodel.UserName,
                //Password = password.Select(x => x.Password).ToString()
              // ConfirmPassword = orignalmodel.PasswordHash
                
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ApplicationUser model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("user is not valid!");
                

                var originalModel = UnitOfWork.Repository<ApplicationUser>().GetById(model.Id);

                if (originalModel == null)
                    throw new Exception("user do not exist!");

                originalModel.UserName = model.UserName;
                originalModel.FirstName = model.FirstName;
                originalModel.LastName = model.LastName;
                originalModel.Email = model.Email;
                originalModel.PasswordHash = model.PasswordHash;
                //originalModel.AcademicYear = model.Department.AcademicYear;

               
                UnitOfWork.Repository<ApplicationUser>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "user", model.UserName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "category", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "user", model.UserName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "user", model.UserName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Lock(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                var now = DateTime.Now;
                now = now.AddMonths(1);

                if (!UserManager.GetLockoutEnabled(id))
                    await UserManager.SetLockoutEnabledAsync(id, true);

                var result = await UserManager.SetLockoutEndDateAsync(id, new DateTimeOffset(now));

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.Lock, "user", model.UserName);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't lock the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Lock, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> UnLock(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                if (!UserManager.GetLockoutEnabled(id))
                    throw new Exception("User can't be unlocked!");

                var result = await UserManager.SetLockoutEndDateAsync(id, DateTimeOffset.MinValue);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.Lock, "user", model.UserName);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't unlock the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Lock, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> EnableTwoFactor(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                var result = IdentityResult.Success;

                if (!UserManager.GetTwoFactorEnabled(id))
                    result = await UserManager.SetTwoFactorEnabledAsync(id, true);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.EnableTwoFactor, "user", model.UserName);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't enable Two Factor the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.EnableTwoFactor, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> DisableTwoFactor(int id)
        {
            try
            {
                var model = await UserManager.FindByIdAsync(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                var result = IdentityResult.Success;

                if (UserManager.GetTwoFactorEnabled(id))
                    result = await UserManager.SetTwoFactorEnabledAsync(id, false);

                if (result.Succeeded)
                {
                    var msg = CreateMessage(ControllerActionType.DisableTwoFactor, "user", model.UserName);

                    if (this.Request.IsAjaxRequest())
                    {
                        return Json(new { state = 1, id = model.Id, message = msg });
                    }
                    else
                    {
                        return RedirectToAction("Index", "User").WithSuccess(msg);
                    }
                }
                else
                {
                    throw new Exception("Can't disable Two Factor the user!");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.DisableTwoFactor, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "User").WithError(msg);
                }
            }
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}