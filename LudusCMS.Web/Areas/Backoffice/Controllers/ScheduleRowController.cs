﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class ScheduleRowController : CommonController
    {
        // GET: Backoffice/Reportcard/Create
        public ActionResult Create(Int16 id, int position)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Schedule");
            }

            var scheduleColumnId= UnitOfWork.Repository<ScheduleColumn>().GetById(id);
            var classId = UnitOfWork.Repository<Class>().GetById(scheduleColumnId.ClassId);
            var departmentId = classId.DepartmentId;

            var model = new ScheduleRowViewModel
            {
                ScheduleRow = new ScheduleRow() { Position = position},
                ScheduleColumn = UnitOfWork.Repository<ScheduleColumn>().GetById(id),
                Courses = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == departmentId),
                Classrooms = UnitOfWork.Repository<Classroom>().GetAll()
            };
            

            return View(model);
        }

        // POST: Backoffice/Reportcard/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ScheduleRowViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("ScheduleRow is not valid!");

                var schedulerow = model.ScheduleRow;
                var schedulecolumnid = model.ScheduleRow.ScheduleColumnId;
                

                UnitOfWork.Repository<ScheduleRow>().Insert(schedulerow);
                int result = UnitOfWork.SaveChanges();


                return RedirectToAction("Details", "Schedule", new { id = schedulecolumnid }).WithSuccess(CreateMessage(ControllerActionType.Create, "reportcardrow", model.ScheduleRow.Id));
            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "schedulerow", ex);

                var viewModel = new ScheduleRowViewModel
                {
                    ScheduleRow = model.ScheduleRow,
                    ScheduleColumn = model.ScheduleColumn,
                    Courses = UnitOfWork.Repository<Course>().GetAll(),
                    Classrooms = UnitOfWork.Repository<Classroom>().GetAll()
                };
                return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "schedulerow", viewModel.ScheduleRow.Id, ex));
            }
        }
        public ActionResult Edit(Int16? id, Int16? columnId)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Reportcard By Id (primary key)
            var model = new ScheduleRowViewModel() {
                ScheduleRow = UnitOfWork.Repository<ScheduleRow>().GetBy(filter: x => x.Id == id).First(),
                ScheduleColumn = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.Id == columnId).First(),
                Classrooms = UnitOfWork.Repository<Classroom>().GetAll(),
                Courses = UnitOfWork.Repository<Course>().GetAll()

            };

            if (model == null)
            {
                return RedirectToAction("Details", "Schedule", new { id = model.ScheduleRow.ScheduleColumnId }).WithSuccess(CreateMessage(ControllerActionType.Create, "schedulerow", model.ScheduleColumn.Id));
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ScheduleRowViewModel model)
        {
            //try
            //{
            //    if (!ModelState.IsValid)
            //        throw new Exception("ScheduleRow is not valid!");

            //    var originalModel = UnitOfWork.Repository<ScheduleRow>().GetById(model.ScheduleRow.Id);

            //   if (originalModel == null)
            //        throw new Exception("ScheduleRow do not exist!");
               

            //    originalModel.CourseId = model.ScheduleRow.CourseId;
            //   originalModel.Position = model.ScheduleRow.Position;
            //   originalModel.ClassroomId = model.ScheduleRow.ClassroomId;
            //    originalModel.Course = model.ScheduleRow.Course;


            //    UnitOfWork.Repository<ScheduleRow>().Update(originalModel);
            //    var result = UnitOfWork.SaveChanges();

            //    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleRow.ScheduleId }).WithSuccess(CreateMessage(ControllerActionType.Edit, "schoolyear", model.ScheduleRow.Id));
            //}
            //catch (Exception ex)
            //{
            //    return View(model).WithError(CreateMessage(ControllerActionType.Create, "schedulerow", model.ScheduleRow.Id, ex));
            //}

            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("schedulerow is not valid!");

                var originalModel = UnitOfWork.Repository<ScheduleRow>().GetById(model.ScheduleRow.Id);

                if (originalModel == null)
                    throw new Exception("schedulerow do not exist!");

                originalModel.Position = model.ScheduleRow.Position;
                originalModel.ScheduleColumnId = model.ScheduleRow.ScheduleColumnId;
                originalModel.CourseId = model.ScheduleRow.CourseId;
                originalModel.ClassroomId = model.ScheduleRow.ClassroomId;
                originalModel.Description = model.ScheduleRow.Description;
               

                UnitOfWork.Repository<ScheduleRow>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Details", "Schedule", new { id = originalModel.ScheduleColumnId});
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }


       
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);

                if (model == null)
                    throw new Exception("schedulerow does not exist!");

                UnitOfWork.Repository<ScheduleRow>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "schedulerow", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.Delete, "schedulerow", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);

                if (model == null)
                    throw new Exception("ReportcardRow does not exist!");

                UnitOfWork.Repository<ScheduleRow>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedulerow", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithError(msg);
                }
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);

                if (model == null)
                    throw new Exception("Schedulerow does not exist!");

                UnitOfWork.Repository<ScheduleRow>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<ScheduleRow>().GetById(id);
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schedule", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", "Schedule", new { id = model.ScheduleColumnId }).WithError(msg);
                }
            }
        }
    }
}