﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class SchoolYearController : CommonController
    {
        // GET: Backoffice/AcademicYear
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<SchoolYear>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        // GET: Backoffice/AcademicYear/Create
        public ActionResult Create()
        {
            var model = new SchoolYear();//Create empty model

            return View(model);
        }

        // POST: Backoffice/AcademicYear/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(SchoolYear model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("SchoolYear is not valid!");


                UnitOfWork.Repository<SchoolYear>().Insert(model);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Create, "schoolyear", model.Name));
            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "schoolyear", model.Name, ex));
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific AcademicYear By Id (primary key)
            var model = UnitOfWork.Repository<SchoolYear>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(SchoolYear model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("SchoolYear is not valid!");

                var originalModel = UnitOfWork.Repository<SchoolYear>().GetById(model.Id);

                if (originalModel == null)
                    throw new Exception("SchoolYear do not exist!");

                originalModel.Name = model.Name;
                originalModel.Description = model.Description;
                originalModel.SchoolYearStart = model.SchoolYearStart;
                originalModel.SchoolYearEnd = model.SchoolYearEnd;

                UnitOfWork.Repository<SchoolYear>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Edit, "schoolyear", model.Name));
            }
            catch (Exception ex)
            {
                return View(model).WithError(CreateMessage(ControllerActionType.Create, "schoolyear", model.Name, ex));
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<SchoolYear>().GetById(id);

                if (model == null)
                    throw new Exception("SchoolYear does not exist!");

                UnitOfWork.Repository<SchoolYear>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "schoolyear", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "schoolyear", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<SchoolYear>().GetById(id);

                if (model == null)
                    throw new Exception("SchoolYear does not exist!");

                UnitOfWork.Repository<SchoolYear>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schoolyear", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "schoolyear", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<SchoolYear>().GetById(id);

                if (model == null)
                    throw new Exception("SchoolYear does not exist!");

                UnitOfWork.Repository<SchoolYear>().VirtualUnDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schoolyear", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "schoolyear", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "SchoolYear").WithError(msg);
                }
            }
        }
    }
}