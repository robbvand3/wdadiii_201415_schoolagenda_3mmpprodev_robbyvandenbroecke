﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using System.Net;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class DepartmentController : CommonController
    {
        // GET: Backoffice/Department
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Department>().GetAll();
            var schoolyear = UnitOfWork.Repository<SchoolYear>().GetAll();
            ViewBag.Schoolyears = schoolyear;

            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }

            return View(model);
        }

        public ActionResult Details(Int16? id)
        {
            var model = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == id);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartialDetails", model);
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteCourse(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);
                var identiteit = model.DepartmentId;
                if (model == null)
                    throw new Exception("Course does not exist!");

                UnitOfWork.Repository<Course>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "course", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Details", new { id= identiteit});
                }

            }
            catch (Exception ex)
            {
                var model = UnitOfWork.Repository<Course>().GetById(id);
                var identiteit = model.DepartmentId;
                var msg = CreateMessage(ControllerActionType.Delete, "course", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                     return RedirectToAction("Details", new { id = identiteit});
                }
            }
        }

        public ActionResult Create()
        {
            var model = new DepartmentViewModel
            {
                Department = new Department(),
                Schoolyears = UnitOfWork.Repository<SchoolYear>().GetAll()
            };
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(DepartmentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Department is not valid!");

                var department = model.Department;

                UnitOfWork.Repository<Department>().Insert(department);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new DepartmentViewModel
                {
                    Department = model.Department,
                    Schoolyears = UnitOfWork.Repository<SchoolYear>().GetAll()
                };
                return View(viewModel);
            }
        }

        public ActionResult Edit(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = new DepartmentViewModel
            {
                Department = UnitOfWork.Repository<Department>().GetById(id),
                Schoolyears = UnitOfWork.Repository<SchoolYear>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(DepartmentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Department is not valid!");

                var originalModel = UnitOfWork.Repository<Department>().GetById(model.Department.Id);

                if (originalModel == null)
                    throw new Exception("Department do not exist!");

                originalModel.Name = model.Department.Name;
                originalModel.Description = model.Department.Description;
                originalModel.SchoolYearId = model.Department.SchoolYearId;
                //originalModel.AcademicYear = model.Department.AcademicYear;

                UnitOfWork.Repository<Department>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().Delete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "department", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "department", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().VirtualDelete(id);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "department", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "department", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(Int16 id)
        {
            try
            {
                var model = UnitOfWork.Repository<Department>().GetById(id);

                if (model == null)
                    throw new Exception("Department does not exist!");

                UnitOfWork.Repository<Department>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "department", model.Name);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "department", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Department");
                }
            }
        }
    }
}