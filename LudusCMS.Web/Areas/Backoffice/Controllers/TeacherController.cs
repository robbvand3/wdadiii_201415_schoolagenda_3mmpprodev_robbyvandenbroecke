﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Web.Infrastructure.Alerts;
using LudusCMS.Web.Areas.Backoffice.Controllers;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class TeacherController : CommonController
    {
        // GET: Backoffice/Student
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(2));

            var classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Classes = classes;


            
            return View(model);
        }

        public ActionResult AddClass(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            var viewModel = new TeacherViewModel
            {
                CurrentTeacher= model,
                Classes = UnitOfWork.Repository<Class>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddClass(TeacherViewModel model, int? id)
        {
            //      try 
            //        {
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ClassId = model.CurrentTeacher.ClassId;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("Index").WithSuccess(CreateMessage(ControllerActionType.Edit, "student", model.CurrentTeacher.UserName));
            //          }
            //  catch (Exception ex)
            //{
            //  var viewModel = new StudentViewModel
            //{
            //  CurrentStudent = model.CurrentStudent,
            //Classes = UnitOfWork.Repository<Class>().GetAll()
            //};

            //return View(viewModel).WithError(CreateMessage(ControllerActionType.Create, "student", model.CurrentStudent.UserName, ex));

            //            }
        }

    }
}