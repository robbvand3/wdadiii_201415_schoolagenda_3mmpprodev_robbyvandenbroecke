﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.Viewmodels;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Areas.Backoffice.Controllers
{
    public class GebruikerController : CommonController
    {
        // GET: Backoffice/Gebruiker
        public ActionResult Index()
        {
            // var model = UnitOfWork.Repository<ApplicationUser>().GetAll();
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => !x.Roles.Select(y => y.RoleId).Contains(1) && !x.Roles.Select(y => y.RoleId).Contains(2) && !x.Roles.Select(y => y.RoleId).Contains(3) && !x.Roles.Select(y => y.RoleId).Contains(4) && !x.Roles.Select(y => y.RoleId).Contains(5) && !x.Roles.Select(y => y.RoleId).Contains(6) && !x.Roles.Select(y => y.RoleId).Contains(7) && !x.Roles.Select(y => y.RoleId).Contains(8) && !x.Roles.Select(y => y.RoleId).Contains(9)); // get all the users that not contains a role
            var departments = UnitOfWork.Repository<Department>().GetAll();

            ViewBag.Gebruikers = model;
            ViewBag.Departments = departments;

            return View();
        }




        public ActionResult Edit(Int16? id)
        {
            var orignalmodel = UnitOfWork.Repository<ApplicationUser>().GetById(id);

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var model = new ApplicationUser()
            {


                FirstName = orignalmodel.FirstName,
                LastName = orignalmodel.LastName,
                Email = orignalmodel.Email,
                UserName = orignalmodel.UserName


            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ApplicationUser model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("user is not valid!");


                var originalModel = UnitOfWork.Repository<ApplicationUser>().GetById(model.Id);

                if (originalModel == null)
                    throw new Exception("user do not exist!");


                originalModel.FirstName = model.FirstName;
                originalModel.LastName = model.LastName;
                originalModel.Email = model.Email;
                originalModel.UserName = model.UserName;

                //originalModel.AcademicYear = model.Department.AcademicYear;


                UnitOfWork.Repository<ApplicationUser>().Update(originalModel);
                var result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }

        }
        public ActionResult AddRole(int id)
        {

            var orignalmodel = UnitOfWork.Repository<ApplicationUser>().GetById(id);
            var model = new UserRoleViewModel
            {
                userrole = new ApplicationUserRole
                {
                    UserId = orignalmodel.Id

                },
                users = UnitOfWork.Repository<ApplicationUser>().GetAll(),
                roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),



            };
            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddRole(UserRoleViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("userrole is not valid!");


                var department = model.userrole;

                UnitOfWork.Repository<ApplicationUserRole>().Insert(department);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                var viewModel = new UserRoleViewModel
                {
                    userrole = model.userrole,

                    roles = UnitOfWork.Repository<ApplicationRole>().GetAll(),
                    users = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };
                return View(viewModel);
            }
        }



        #region Delete roles
        public ActionResult DeleteRoleTeacher(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();

           

            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexTeachers");

        }

        public ActionResult DeleteRoleStudent(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexStudents");

        }

        public ActionResult DeleteRoleAdmins(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexAdmins");

        }

        public ActionResult DeleteRoleClasstitular(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexClasstitulars");

        }

        public ActionResult DeleteRoleParent(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexParents");

        }

        public ActionResult DeleteRolePrincipal(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexPrincipals");

        }

        public ActionResult DeleteRoleSecretary(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexSecretaries");

        }

        public ActionResult DeleteRoleCoordinator(UserRoleViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUserRole>().GetBy(filter: x => x.UserId == (Int32)id).First();



            UnitOfWork.Repository<ApplicationUserRole>().Delete(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexCoordinators");

        }
        #endregion 




        public ActionResult AddClass(int? id, int? departmentId)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            var viewModel = new StudentViewModel
            {
                CurrentStudent = model,
                Classes = UnitOfWork.Repository<Class>().GetBy(filter: x => x.DepartmentId == departmentId)
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddClass(StudentViewModel model, int? id)
        {
            //      try 
            //        {
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ClassId = model.CurrentStudent.ClassId;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexStudents").WithSuccess(CreateMessage(ControllerActionType.Edit, "student", model.CurrentStudent.UserName));
           
        }


        public ActionResult AddClassTitular(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            var viewModel = new StudentViewModel
            {
                CurrentStudent = model,
                Classes = UnitOfWork.Repository<Class>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddClassTitular(StudentViewModel model, int? id)
        {
            //      try 
            //        {
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ClassId = model.CurrentStudent.ClassId;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexClasstitulars").WithSuccess(CreateMessage(ControllerActionType.Edit, "student", model.CurrentStudent.UserName));

        }
        

       
        public ActionResult DeleteClass(StudentViewModel model, int? id)
        {
           
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ClassId = null;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexStudents");
           
        }

        public ActionResult DeleteClassTitular(StudentViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ClassId = null;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexClasstitulars");

        }




        public ActionResult AddStudent(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            var viewModel = new ParentViewModel
            {
                CurrentParent = model,
                AllStudents = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(1))
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }



        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddStudent(ParentViewModel model, int? id)
        {
           
            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ChildId = model.CurrentParent.ChildId;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexParents").WithSuccess(CreateMessage(ControllerActionType.Edit, "parent", model.CurrentParent.UserName));
           
        }

        
        public ActionResult DeleteStudent(ParentViewModel model, int? id)
        {

            var originalModel = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == (Int32)id).First();

            originalModel.ChildId = null;

            UnitOfWork.Repository<ApplicationUser>().Update(originalModel);

            var result = UnitOfWork.SaveChanges();

            return RedirectToAction("IndexParents");

        }






        public ActionResult IndexStudents()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(1));
            var classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Students = model;
            ViewBag.Classes = classes;

            return View();
        }

        public ActionResult IndexAdmins()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(5));


            ViewBag.Admins = model;


            return View();
        }

        public ActionResult IndexTeachers()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(2));
            var classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Teachers = model;
            ViewBag.Classes = classes;


            return View();
        }

        public ActionResult IndexParents()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(3));
            var students = UnitOfWork.Repository<ApplicationUser>().GetAll();

            ViewBag.Parents = model;
            ViewBag.Students = students;


            return View();
        }

        public ActionResult IndexClasstitulars()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(6));
            var Classes = UnitOfWork.Repository<Class>().GetAll();

            ViewBag.Classtitulars = model;
            ViewBag.Classes = Classes;


            return View();
        }

        public ActionResult IndexSecretaries()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(7));


            ViewBag.Secretaries = model;


            return View();
        }

        public ActionResult IndexPrincipals()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(8));


            ViewBag.Principals = model;


            return View();
        }

        public ActionResult IndexCoordinators()
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Roles.Select(y => y.RoleId).Contains(9));


            ViewBag.Coordinators = model;


            return View();
        }




        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().Delete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.Delete, "user", model.Id);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.Delete, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualDelete, "user", model.FirstName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualDelete, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithError(msg);
                }
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult VirtualUnDelete(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (model == null)
                    throw new Exception("User does not exist!");

                UnitOfWork.Repository<ApplicationUser>().VirtualUnDelete(model);
                int result = UnitOfWork.SaveChanges();

                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "user", model.FirstName);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 1, id = model.Id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithSuccess(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CreateMessage(ControllerActionType.VirtualUnDelete, "user", id, ex);

                if (this.Request.IsAjaxRequest())
                {
                    return Json(new { state = 0, id = id, message = msg });
                }
                else
                {
                    return RedirectToAction("Index", "Gebruiker").WithError(msg);
                }
            }
        }

    }
}