﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LudusCMS.Web.Areas.
    
    ffice
{
    public class BackofficeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Backoffice";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Backoffice_default",
                "Backoffice/{controller}/{action}/{id}",
                defaults:new { controller = "Home",action = "Index", id = UrlParameter.Optional},
                namespaces:new[]{"LudusCMS.Web.Areas.Backoffice.Controllers"}).DataTokens["UseNamespaceFallback" ] = false;
                 
        }
    }
}