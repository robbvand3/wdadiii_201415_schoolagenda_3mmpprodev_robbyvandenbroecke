﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LudusCMS.Web.Startup))]
namespace LudusCMS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
