﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Infrastructure.Alerts;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Controllers
{
    public class MessageController :CommonController
    {
        //Postvak In 
        public ActionResult Index()
        {
            //Get All Messages
            int id = Convert.ToInt16(User.Identity.GetUserId());
            var model = UnitOfWork.Repository<Message>().GetBy(filter: x => x.ReceiverId == id);
            model = model.OrderByDescending(m => m.Created);

            return View(model);
        }


        public ActionResult Details(Int16? id)
        {
            ViewBag.Users = UnitOfWork.Repository<ApplicationUser>().GetAll();
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var model = new MessageViewModel
            {
                Message = UnitOfWork.Repository<Message>().GetById(id),
                Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()
                
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Backoffice/Meesage/Create
        public ActionResult Create()
        {
            var model = new MessageViewModel
            {
                Message = new Message()
                {

                    SenderId = Convert.ToInt16(User.Identity.GetUserId())
                },
                Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()
            };

            return View(model);
        }

        // POST: Backoffice/Message/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(MessageViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Message is not valid!");

                var message = model.Message;

                UnitOfWork.Repository<Message>().Insert(message);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new MessageViewModel
                {
                    Message = model.Message,
                    Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };

                return View(viewModel);
            }
        }


        public ActionResult Autocomplete(string term)
        {
        try
        {
            var model = UnitOfWork.Repository<ApplicationUser>().GetAll()  // your data here
            .Where(p => p.Email.StartsWith(term))
            .Take(5)
            .Select(p => new
            {
                // jQuery UI needs the label property to function 
                label = p.Email.Trim(),
                value = p.Id
                
            });

        // Json returns [{"label":value}]
            return Json(model.Select(m => new
            {
                id = m.value,
                value = m.value,
                label = m.label,
            }), JsonRequestBehavior.AllowGet);
        }
        catch (Exception ex)
        {
        
        return Json("{'ex':'Exception'}");
        }
    }

        //Postvack Out
        public ActionResult IndexSended()
        {
            //Get All Messages
            int id = Convert.ToInt16(User.Identity.GetUserId());
            var model = UnitOfWork.Repository<Message>().GetBy(filter: x => x.SenderId == id);
            model = model.OrderByDescending(m => m.Created);

            return View(model);
        }

       
        public ActionResult SendedDetails(Int16? id)
        {
            ViewBag.Users = UnitOfWork.Repository<ApplicationUser>().GetAll();
            if (id == null)
            {
                return RedirectToAction("IndexSended");
            }

            var model = new MessageViewModel
            {
                Message = UnitOfWork.Repository<Message>().GetById(id),
                Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()

            };

            if (model == null)
            {
                return RedirectToAction("IndexSended");
            }

            return View(model);
        }
        public ActionResult Reply(Int16? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var originalModel = UnitOfWork.Repository<Message>().GetById(id);
            var model = new MessageViewModel
            {
                Message = new Message{
                    SenderId= Convert.ToInt16(User.Identity.GetUserId()),
                    ReceiverId = originalModel.SenderId,
                    Title = "Re:" + originalModel.Title

                },
                Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()
            };

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Reply(MessageViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new Exception("Message is not valid!");

                var message = model.Message;

                UnitOfWork.Repository<Message>().Insert(message);
                int result = UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var viewModel = new MessageViewModel
                {
                    Message = model.Message,
                    Receivers = UnitOfWork.Repository<ApplicationUser>().GetAll()
                };

                return View(viewModel);
            }
        }
       


        #region Only Partial Views
        //Postvak In
        public ActionResult PVGetMessages(int? page, int? pageSize)
        {
            int id = Convert.ToInt16(User.Identity.GetUserId());
            var pageNumber = page ?? 1;
            var itemsPerPage = pageSize ?? 15;

            var model = UnitOfWork.Repository<Message>().GetBy(filter: x => x.ReceiverId == id);
            model = model.OrderByDescending(m => m.Updated);

            var pagedModel = model.ToPagedList(pageNumber, itemsPerPage);

            return PartialView("_ListMessages", pagedModel);
        }

        //Postvak Out

        public ActionResult PVGetSendedMessages(int? page, int? pageSize)
        {
            int id = Convert.ToInt16(User.Identity.GetUserId());
            var pageNumber = page ?? 1;
            var itemsPerPage = pageSize ?? 15;

            var model = UnitOfWork.Repository<Message>().GetBy(filter: x => x.SenderId == id);
            model = model.OrderByDescending(m => m.Updated);

            var pagedModel = model.ToPagedList(pageNumber, itemsPerPage);

            return PartialView("_ListSendedMessages", pagedModel);
        }
        #endregion

        
    }
}