﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Controllers
{
    public class ScheduleController : CommonController
    {
        public ActionResult Index()
        {
            var model = UnitOfWork.Repository<Schedule>().GetAll();
            model = model.OrderByDescending(m => m.Created);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("_ListPartial", model);
            }


            return View(model);
        }
        //
        // GET: /Schedules/

        public ActionResult Columns(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            //Get a specific Post By Id (primary key)
            var schedule = UnitOfWork.Repository<Schedule>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Schedule, object>>> { z => z.ScheduleColumns }).First();

            var classes = UnitOfWork.Repository<Class>().GetAll();


            if (schedule == null)
            {
                return RedirectToAction("Index");
            }


            var model = new ScheduleViewModel
            {
                Schedule = schedule,
                ScheduleColumn = schedule.ScheduleColumns,

                Classes = classes,

            };

            return View(model);
        }


        public ActionResult Details(int? id)
        {
            try
            {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            int userId = Convert.ToInt16(User.Identity.GetUserId());

            var user = UnitOfWork.Repository<ApplicationUser>().GetById(userId);

            if (user.ClassId != null)
            {
                var classId = user.ClassId;
                var columnId = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.ClassId == classId && x.ScheduleId == id).First();

                
                var schedulecolumn = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.Id == columnId.Id, includes: new List<System.Linq.Expressions.Expression<Func<ScheduleColumn, object>>> { y => y.Class, z => z.ScheduleRows }).First();
                var scheduleColumnClass = UnitOfWork.Repository<Class>().GetById(schedulecolumn.ClassId);
                var classes = UnitOfWork.Repository<Class>().GetAll();
                var classrooms = UnitOfWork.Repository<Classroom>().GetAll();
                var courses = UnitOfWork.Repository<Course>().GetAll();
                if (schedulecolumn == null)
                {
                    return RedirectToAction("Index");
                }
                var model = new ScheduleColumnViewModel
                {
                    ScheduleColumn = schedulecolumn,
                    Class = scheduleColumnClass,
                    ScheduleRow = schedulecolumn.ScheduleRows,
                    Classes = classes,
                    Classrooms = classrooms,
                    Courses = courses
                };

                return View(model);
            }

            else 
            {
                var childId = user.ChildId;
                var child = UnitOfWork.Repository<ApplicationUser>().GetById(childId);               
                var classId = child.ClassId;

                var columnId = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.ClassId == classId && x.ScheduleId == id).First();


                var schedulecolumn = UnitOfWork.Repository<ScheduleColumn>().GetBy(filter: x => x.Id == columnId.Id, includes: new List<System.Linq.Expressions.Expression<Func<ScheduleColumn, object>>> { y => y.Class, z => z.ScheduleRows }).First();
                var scheduleColumnClass = UnitOfWork.Repository<Class>().GetById(schedulecolumn.ClassId);
                var classes = UnitOfWork.Repository<Class>().GetAll();
                var classrooms = UnitOfWork.Repository<Classroom>().GetAll();
                var courses = UnitOfWork.Repository<Course>().GetAll();
                if (schedulecolumn == null)
                {
                    return RedirectToAction("Index");
                }
                var model = new ScheduleColumnViewModel
                {
                    ScheduleColumn = schedulecolumn,
                    Class = scheduleColumnClass,
                    ScheduleRow = schedulecolumn.ScheduleRows,
                    Classes = classes,
                    Classrooms = classrooms,
                    Courses = courses
                };

                return View(model);
            
                
            }
            }
           
           catch
                {
                    return RedirectToAction("Error");
                }
        }

        public ActionResult Error()
        {
            return View();
        }
       

        public ActionResult PVGetScheduleRowByPostition(int position, int id)
        {


            var model = UnitOfWork.Repository<ScheduleRow>().GetBy(
                filter: f => f.Position == position && f.ScheduleColumnId == id,
               orderBy: m => m.OrderBy(p => p.Id)
                );

            var classrooms = UnitOfWork.Repository<Classroom>().GetAll();
            var courses = UnitOfWork.Repository<Course>().GetAll();
            var teachers = UnitOfWork.Repository<ApplicationUser>().GetAll();

            ViewBag.Classrooms = classrooms;
            ViewBag.Courses = courses;
            ViewBag.Teachers = teachers;

            return PartialView("_ListScheduleRow", model);
        }
    }
}