﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Controllers
{
    public class CourseController : CommonController
    {
        //
        // GET: /Cursussen/
        public ActionResult Index()
        {
            //Get All Events
            int id = Convert.ToInt16(User.Identity.GetUserId());
            
            var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);
           

            try
            {
                

                if (user.ChildId == null)
                {
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == user.DepartmentId).First();

                    var model = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == userdepartment.Id);
                    model = model.OrderByDescending(m => m.Name);

                    return View(model);
                }
                else
                {
                    var child = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == user.ChildId).First();
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == child.DepartmentId).First();

                    var model = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == userdepartment.Id);
                    model = model.OrderByDescending(m => m.Name);

                    return View(model);
                }
                
            }
            catch
            {
                return RedirectToAction("Error", "Course");
            }
        }

        public ActionResult Error()
        {
            return View();
        }
        public ActionResult Details(int? id)
        {
            var teachers = UnitOfWork.Repository<ApplicationUser>().GetAll();
            ViewBag.Teachers = teachers;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get a specific Post By Id (primary key)
            var model = UnitOfWork.Repository<Course>().GetById(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        #region Only Partial Views
        public ActionResult PVGetCourses()
        {


            int id = Convert.ToInt16(User.Identity.GetUserId());

            var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);
           

            try
            {
                if (user.ChildId == null)
                {
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == user.DepartmentId).First();
                   

                    var model = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == userdepartment.Id);
                    model = model.OrderByDescending(m => m.Name);

                    return PartialView("_ListCourses", model);
                }
                else
                {
                    var child = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == user.ChildId).First();
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == child.DepartmentId).First();

                    var model = UnitOfWork.Repository<Course>().GetBy(filter: x => x.DepartmentId == userdepartment.Id);
                    model = model.OrderByDescending(m => m.Name);

                    return PartialView("_ListCourses", model);
                }
               

               
            }
            catch
            {
                return RedirectToAction("Error", "Course");
            }

        }

        public ActionResult PVGetEvents(int courseId)
        {

            int id = Convert.ToInt16(User.Identity.GetUserId());
            var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);
            var userclass = UnitOfWork.Repository<Class>().GetBy(filter: x => x.Id == user.ClassId ).First();

            var model = UnitOfWork.Repository<Event>().GetBy(
                 filter: f => f.CourseId == courseId && f.ClassId == userclass.Id,
                  orderBy: m => m.OrderBy(p => p.Start_Date)
                 );



            return PartialView("_ListEvents", model);
        }




        #endregion
    }
}