﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;

namespace LudusCMS.Web.Controllers
{
    public enum RenderViewType
    {
        SMALL,
        MEDIUM,
        MEDIUMFLOAT,
        LARGE
    }
    public class ComponentController : CommonController
    {
        public ActionResult CompGetPosts(int? amount = null, RenderViewType rvType = RenderViewType.SMALL)
        {
            var model = UnitOfWork.Repository<Blog>().GetBy(orderBy: p => p.OrderByDescending(m => m.Updated), page: 1, pageSize: amount);

            return PartialView(GetPartialViewByRenderViewType("Posts", rvType), model);
        }

        public ActionResult CompGetPostsByDepartment(int departmentId, int? amount = null, RenderViewType rvType = RenderViewType.SMALL)
        {
            var model = UnitOfWork.Repository<Blog>().GetBy(filter: m => m.DepartmentId == departmentId, orderBy: p => p.OrderByDescending(m => m.Updated), page: 1, pageSize: amount);

            return PartialView(GetPartialViewByRenderViewType("Blogs", rvType), model);
        }

        public ActionResult CompGetDepartments(int? amount = null, RenderViewType rvType = RenderViewType.SMALL)
        {
            var model = UnitOfWork.Repository<Department>().GetBy(orderBy: p => p.OrderBy(m => m.Name), page: 1, pageSize: amount);

            return PartialView(GetPartialViewByRenderViewType("Departments", rvType), model);
        }

        // public ActionResult CompGetMainCategories(int? amount = null, RenderViewType rvType = RenderViewType.SMALL)
        // {
        //  var model = UnitOfWork.Repository<Category>().GetBy(filter: p => p.ParentCategoryId == null, includes: new List<System.Linq.Expressions.Expression<Func<Category, object>>> { p => p.ChildCategories }, orderBy: p => p.OrderBy(x => x.Name), page: 1, pageSize: amount);

        // return PartialView(GetPartialViewByRenderViewType("Categories", rvType), model);
        //}

        // public ActionResult CompGetCommentsByPost(int postId, int? amount = null, RenderViewType rvType = RenderViewType.MEDIUM)
        //{
        // var model = UnitOfWork.Repository<Comment>().GetBy(filter: m => m.PostId == postId, orderBy: p => p.OrderByDescending(m => m.Updated), page: 1, pageSize: amount);

        //return PartialView(GetPartialViewByRenderViewType("Comments", rvType), model);
        // }

        #region Helpers
        private string GetPartialViewByRenderViewType(string modelType, RenderViewType rvType)
        {
            switch (rvType)
            {
                case RenderViewType.SMALL:
                default: return "_SmallList" + modelType;
                case RenderViewType.MEDIUM: return "_MediumList" + modelType;
                case RenderViewType.MEDIUMFLOAT: return "_MediumListFloat" + modelType;
                case RenderViewType.LARGE: return "_LargeList" + modelType;
            }
        }
        #endregion
    }
}