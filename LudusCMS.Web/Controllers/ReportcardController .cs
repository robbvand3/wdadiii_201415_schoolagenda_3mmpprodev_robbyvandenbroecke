﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;

using Microsoft.AspNet.Identity;



namespace LudusCMS.Web.Controllers
{
    public class ReportcardController : CommonController
    {
        //
        // GET: /Reportcards/
        public ActionResult Index()
        {
            int id = Convert.ToInt16(User.Identity.GetUserId());
            var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);
            ViewBag.Students = UnitOfWork.Repository<ApplicationUser>().GetAll();
            try
            {
                if (user.ChildId == null)
                {
                    var model = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.UserId == user.Id);
                    model = model.OrderBy(m => m.Created);

                    return View(model);
                }

                else 
                {
                    var child = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == user.ChildId).First();

                    var model = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.UserId == child.Id);
                    model = model.OrderBy(m => m.Created);

                    return View(model);
                }
            }
            catch
            {
                return RedirectToAction("Error", "Reportcard");
            }


        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Get a specific Post By Id (primary key)
            var model = UnitOfWork.Repository<Reportcard>().GetById(id);

            var classes = UnitOfWork.Repository<Course>().GetAll();

            ViewBag.Classes = classes;

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        #region Only Partial Views
        public ActionResult PVGetReportcards()
        {
            try
            {
                ViewBag.Users = UnitOfWork.Repository<ApplicationUser>().GetAll();
                int id = Convert.ToInt16(User.Identity.GetUserId());
                var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                if (user.ChildId == null)
                {
                    var model = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.UserId == user.Id, orderBy: m => m.OrderBy(p => p.Created));


                    var classes = UnitOfWork.Repository<Course>().GetAll();

                    ViewBag.Classes = classes;

                    return PartialView("_ListReportcards", model);
                }
                else
                {
                    var child = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == user.ChildId).First();

                    var model = UnitOfWork.Repository<Reportcard>().GetBy(filter: x => x.UserId == child.Id);
                    model = model.OrderBy(m => m.Created);


                    var classes = UnitOfWork.Repository<Course>().GetAll();

                    ViewBag.Classes = classes;

                    return PartialView("_ListReportcards", model);
                }

                
                

                
            }
            catch
            {
                return RedirectToAction("Error");
            }

            
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Sign(int? id)
        {
            
                        var model = UnitOfWork.Repository<Reportcard>().GetById(id);

                        if (model == null)
                            throw new Exception("Reportcard does not exist!");
                        int userId = Convert.ToInt16(User.Identity.GetUserId());
                        var user = UnitOfWork.Repository<ApplicationUser>().GetById(userId);
                        
                        
                        model.Signature1 = userId;

                        UnitOfWork.Repository<Reportcard>().Update(model);
                        int result = UnitOfWork.SaveChanges();

                        
                        
                            return RedirectToAction("Index", "Reportcard");
                        



        }

        public ActionResult Sign2(int? id)
        {

            var model = UnitOfWork.Repository<Reportcard>().GetById(id);

            if (model == null)
                throw new Exception("Reportcard does not exist!");
            int userId = Convert.ToInt16(User.Identity.GetUserId());
            var user = UnitOfWork.Repository<ApplicationUser>().GetById(userId);


            model.Signature2 = userId;

            UnitOfWork.Repository<Reportcard>().Update(model);
            int result = UnitOfWork.SaveChanges();



            return RedirectToAction("Index", "Reportcard");




        }

        public ActionResult PVGetReportcardRows(int id)
        {
            try
            {
                var model = UnitOfWork.Repository<ReportcardRow>().GetBy(
                filter: f => f.ReportcardId == id,
                orderBy: m => m.OrderBy(p => p.Id)
                );

                int aantalVakken = model.Count();
                ViewBag.Count = aantalVakken;
                
                decimal totalescore = 0;

                foreach( var vak in model)
                {
                 
                    totalescore += vak.TotalScore;
                }

                if (aantalVakken != 0)
                {

                    decimal gemiddelde = totalescore / aantalVakken;


                    ViewBag.gemiddelde = Math.Round(gemiddelde, 2);

                    var classes = UnitOfWork.Repository<Course>().GetAll();

                    ViewBag.Classes = classes;

                    return PartialView("_ListReportcardRows", model);
                }

                else 
                {
                    return PartialView("ErrorPunten");
                }
            }
            catch
            {
                return RedirectToAction("ErrorPunten");
            }
            
       
        }

        #endregion
    }
}