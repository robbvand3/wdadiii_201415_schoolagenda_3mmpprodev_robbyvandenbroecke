﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

using DayPilot.Web.Mvc;
using DayPilot.Web.Mvc.Data;
using DayPilot.Web.Mvc.Enums;
using DayPilot.Web.Mvc.Events;
using DayPilot.Web.Mvc.Events.Common;
using DayPilot.Web.Mvc.Events.Month;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace LudusCMS.Web.Controllers
{
     public class CalendarController : CommonController 
    {
        public  ActionResult Index()
        {
            var events = UnitOfWork.Repository<Event>().GetAll();
            return View();
        }

        // GET: Calendar
        public ActionResult Backend()
        {
            UnitOfWork.Repository<ApplicationUser>().GetAll();
            return new Dpm().CallBack(this);
        }

        

        public class Dpm : DayPilotMonth 
        {
            protected override void OnInit(InitArgs initArgs)
            {
                Events = new EventManager().FilteredData(VisibleStart, VisibleEnd).AsEnumerable();

                DataStartField = "Start_Date";
                DataEndField = "End_Date";
                DataTextField = "Name";
                DataIdField = "Id";
                
                Update();
            }

            protected override void OnCommand(CommandArgs e)
            {
                switch (e.Command)
                {
                    case "navigate":
                        StartDate = (DateTime)e.Data["Start_Date"];
                        Update(CallBackUpdateType.Full);
                        break;
                    case "previous":
                       StartDate = StartDate.AddMonths(-1);
                        Update(CallBackUpdateType.Full);
                        break;
                    case "next":
                        StartDate = StartDate.AddMonths(1);
                        Update(CallBackUpdateType.Full);
                        break;
                }
            }

          

            protected override void OnFinish()
            {
                // only load the data if an update was requested by an Update() call
                if (UpdateType == CallBackUpdateType.None)
                {
                    return;
                }

                Events = new EventManager().FilteredData(VisibleStart, VisibleEnd).AsEnumerable();

                DataStartField = "Start_Date";
                DataEndField = "End_Date";
                DataTextField = "Name";
                DataIdField = "Id";
    
            }

            protected override void OnEventBubble(EventBubbleArgs e)
            {
                e.BubbleHtml = "Event details for id: " + e.End;
            }

            
        }














        public class EventManager
        {


            public DataTable FilteredData(DateTime Start_Date, DateTime End_Date)
            {



                var id = System.Web.HttpContext.Current.User.Identity.GetUserId();
                string constring = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constring))
                {
                    

                        using (SqlCommand cmd = new SqlCommand("SELECT DepartmentId FROM [AspNetUsers] WHERE [Id] = @id", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@id", id);
                            con.Open();
                            object o = cmd.ExecuteScalar();
                            if (o != null)
                            {
                                string city = o.ToString();

                                if (city == "")
                                {
                                    using (SqlCommand cmd2 = new SqlCommand("SELECT ChildId FROM [AspNetUsers] WHERE [Id] = @id", con))
                                    {
                                        cmd2.CommandType = CommandType.Text;
                                        cmd2.Parameters.AddWithValue("@id", id);

                                        object o2 = cmd2.ExecuteScalar();
                                        string city2 = o2.ToString();

                                        using (SqlCommand cmd3 = new SqlCommand("SELECT DepartmentId FROM [AspNetUsers] WHERE [Id] = @childId", con))
                                        {
                                            cmd3.CommandType = CommandType.Text;
                                            cmd3.Parameters.AddWithValue("@childId", city2);

                                            object o3 = cmd3.ExecuteScalar();
                                            if (o3 != null)
                                            {
                                                string city3 = o3.ToString();

                                                using (SqlCommand cmdd = new SqlCommand("SELECT ClassId FROM [AspNetUsers] WHERE [Id] = @childId", con))
                                                {
                                                    cmdd.CommandType = CommandType.Text;
                                                    cmdd.Parameters.AddWithValue("@childId", city2);

                                                    object oo = cmdd.ExecuteScalar();
                                                    if (oo != null)
                                                    {
                                                        string klasid = oo.ToString();

                                                        SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE [DepartmentId] = @departmentid AND  [ClassId] = @classid", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                        da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                        da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                        da.SelectCommand.Parameters.AddWithValue("@departmentid", city3);
                                                        da.SelectCommand.Parameters.AddWithValue("@classid", klasid);


                                                        SqlDataAdapter da2 = new SqlDataAdapter("SELECT * FROM [Events] WHERE [DepartmentId] = @departmentid AND  [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                        da2.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                        da2.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                        da2.SelectCommand.Parameters.AddWithValue("@departmentid", city3);


                                                        SqlDataAdapter da3 = new SqlDataAdapter("SELECT * FROM [Events] WHERE [DepartmentId] IS NULL AND  [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                        da3.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                        da3.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                        


                                                        DataTable dt = new DataTable();
                                                        da.Fill(dt);
                                                        da2.Fill(dt);
                                                        da3.Fill(dt);


                                                        return dt;
                                                    }
                                                    else
                                                    {



                                                        SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] = @departmentid AND [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                                da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                                da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                                da.SelectCommand.Parameters.AddWithValue("@departmentid", city3);


                                                                SqlDataAdapter da3 = new SqlDataAdapter("SELECT * FROM [Events] WHERE [DepartmentId] IS NULL AND  [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                                da3.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                                da3.SelectCommand.Parameters.AddWithValue("end", End_Date);

                                                                DataTable dt = new DataTable();
                                                                da.Fill(dt);
                                                                da3.Fill(dt);

                                                                return dt;
                                                            }
                                                }
                                            }
                                                            else
                                                            {
                                                                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                                da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                                da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                                


                                                                DataTable dt = new DataTable();
                                                                da.Fill(dt);

                                                                return dt;
                                                            }
                                                        }
                                                    }
                                                }
                                            
                                            else
                                            {

                                     using (SqlCommand cmdo = new SqlCommand("SELECT ClassId FROM [AspNetUsers] WHERE [Id] = @id", con))
                                                {
                                                    cmdo.CommandType = CommandType.Text;
                                                    cmdo.Parameters.AddWithValue("@id", id);

                                                    object userklas = cmdo.ExecuteScalar();
                                                    if (userklas != null)
                                                    {
                                                        string userklasid = userklas.ToString();




                                                        SqlDataAdapter da2 = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] = @departmentid AND [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                         da2.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                         da2.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                         da2.SelectCommand.Parameters.AddWithValue("@departmentid", city);

                                                         SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] = @departmentid AND [ClassId] = @classid", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                         da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                         da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                         da.SelectCommand.Parameters.AddWithValue("@departmentid", city);
                                                         da.SelectCommand.Parameters.AddWithValue("@classid", userklasid);

                                                         SqlDataAdapter da3 = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] IS NULL AND [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                         da3.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                         da3.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                         



                                                DataTable dt = new DataTable();
                                                da3.Fill(dt);
                                                da.Fill(dt);
                                                da2.Fill(dt);
                                                

                                                return dt;
                                            }
                                                    else
                                                    {
                                                        SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] = @departmentid AND [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                                         da.SelectCommand.Parameters.AddWithValue("@departmentid", city);

                                                         SqlDataAdapter da3 = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] IS NULL AND [ClassId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                                         da3.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                                         da3.SelectCommand.Parameters.AddWithValue("end", End_Date);


                                                DataTable dt = new DataTable();
                                                da.Fill(dt);
                                                da3.Fill(dt);

                                                return dt;
                                                    }
                                        }
                                    }
                                }
                                else
                                {
                                    SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Events] WHERE  [DepartmentId] IS NULL", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                    da.SelectCommand.Parameters.AddWithValue("start", Start_Date);
                                    da.SelectCommand.Parameters.AddWithValue("end", End_Date);
                                 


                                    DataTable dt = new DataTable();
                                    da.Fill(dt);

                                    return dt;
                                }

                            con.Close();
                            }
                
                            
                            
                        
                    }



                
            }
        }
    }
}