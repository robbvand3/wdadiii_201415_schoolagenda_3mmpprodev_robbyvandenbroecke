﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using LudusCMS.Web.Infrastructure.Alerts;

namespace LudusCMS.Web.Controllers
{
    public class BlogController : CommonController
    {
        //
        // GET: /Blogs/
        public ActionResult Index()
        {
            //Get All Blogs
            var model = UnitOfWork.Repository<Blog>().GetBy(includes: new List<System.Linq.Expressions.Expression<Func<Blog, object>>> { x => x.Tags, y => y.Categories, z => z.Department });
            model = model.OrderByDescending(m => m.Created);

            return View(model);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return Redirect("Index");
            }

            //Get a specific Blog By Id (primary key)
            var blog = UnitOfWork.Repository<Blog>().GetBy(filter: x => x.Id == (Int32)id, includes: new List<System.Linq.Expressions.Expression<Func<Blog, object>>> { x => x.Tags, y => y.Categories, z => z.Department }).First();

            if (blog == null)
            {
                return RedirectToAction("Index");
            }


            var filteredCategories = UnitOfWork.Repository<Category>().GetAll();
            var filteredTags = UnitOfWork.Repository<Tag>().GetAll();


            var model = new BlogViewModel
            {
                Blog = blog,
                Departments = UnitOfWork.Repository<Department>().GetAll(),
                Tags = new MultiSelectList(filteredTags, "Id", "Name"),
                Categories = new MultiSelectList(filteredCategories, "Id", "Name"),
            };

            return View(model);
        }
        #region Only Partial Views
        public ActionResult PVGetBlogs(int? page, int? pageSize)
        {
            var pageNumber = page ?? 1;
            var itemsPerPage = pageSize ?? 10;

            var model = UnitOfWork.Repository<Blog>().GetBy(orderBy: m => m.OrderByDescending(p => p.Updated));

            var pagedModel = model.ToPagedList(pageNumber, itemsPerPage);

            return PartialView("_ListBlogs", pagedModel);
        }

        [HttpGet]
        public ActionResult CreateCommentForBlog(int blogId, int? userId, string ajaxUpdateTargetId, int? parentCommentId)
        {
            var model = new CreateCommentForBlogViewModel
            {
                Comment = new LudusCMS.Model.Comment(),
                BlogId = blogId,
                UserId = userId ?? null,
                AJAXUpdateTargetId = ajaxUpdateTargetId,
                ParentCommentId = parentCommentId ?? null
            };

            return PartialView("_CreateCommentForBlog", model);
        }

        [HttpPost]
        public ActionResult CreateCommentForBlog(CreateCommentForBlogViewModel model)
        {
            if (!ModelState.IsValid)
                throw new Exception("Model is not valid!");

            var modelToSave = new Comment()
            {
                Content = model.Comment.Content,
                BlogId = model.BlogId,
                UserId = model.UserId,
                ParentCommentId = model.ParentCommentId
            };

            UnitOfWork.Repository<Comment>().Insert(modelToSave);
            var result = UnitOfWork.SaveChanges();

            return GetCommentsByBlogAsPagedList(model.BlogId, model.UserId, 1, 10);
        }

        public ActionResult GetCommentsByBlogAsPagedList(int blogId, int? userId, int? page, int? pageSize)
        {
            int pageNumber = page ?? 1;
            int itemsPerPage = pageSize ?? 10;

            var model = UnitOfWork.Repository<Comment>().GetBy(
                    filter: f => f.BlogId == blogId,
                    orderBy: m => m.OrderByDescending(p => p.Id)
                );

            var pagedModel = model.ToPagedList(pageNumber, itemsPerPage);

            var viewModel = new CommentsByBlogViewModel()
            {
                Comments = pagedModel,
                BlogId = blogId,
                UserId = userId
            };

            var users = UnitOfWork.Repository<ApplicationUser>().GetAll();

            ViewBag.Users = users;

            return PartialView("_PagedListCommentsByBlog", viewModel);
        }
        #endregion
    }
}