﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LudusCMS.Model;
using LudusCMS.Model.ViewModels;
using Microsoft.AspNet.Identity;

namespace LudusCMS.Web.Controllers
{
    public class EventController : CommonController
    {
        //
        // GET: /Events/
        public ActionResult Index()
        {
            try
            {
                int id = Convert.ToInt16(User.Identity.GetUserId());

                var user = UnitOfWork.Repository<ApplicationUser>().GetById(id);

                var events = UnitOfWork.Repository<Event>().GetAll();

                

                var categories = UnitOfWork.Repository<EventCategory>().GetAll();

               
                ViewBag.Events = events;
                ViewBag.Categories = categories;



                if (user.ChildId == null)
                {
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == user.DepartmentId).First();
                    var userclass = UnitOfWork.Repository<Class>().GetBy(filter: x => x.Id == user.ClassId).First();
                    var model = UnitOfWork.Repository<Event>().GetBy(filter: x => x.DepartmentId == userdepartment.Id && x.ClassId == userclass.Id || x.DepartmentId == userdepartment.Id && x.ClassId == null || x.DepartmentId == null );
                   
                    model = model.OrderByDescending(m => m.Start_Date);

                    return View(model);
                }
                else
                {
                    var child = UnitOfWork.Repository<ApplicationUser>().GetBy(filter: x => x.Id == user.ChildId).First();
                    var userdepartment = UnitOfWork.Repository<Department>().GetBy(filter: x => x.Id == child.DepartmentId).First();
                    var userclass = UnitOfWork.Repository<Class>().GetBy(filter: x => x.Id == child.ClassId).First();

                    var model = UnitOfWork.Repository<Event>().GetBy(filter: x => x.DepartmentId == userdepartment.Id && x.ClassId == userclass.Id || x.DepartmentId == userdepartment.Id && x.ClassId == null || x.DepartmentId == null);
                    model = model.OrderByDescending(m => m.Start_Date);

                    return View(model);
                }



            }
            catch
            {
                return RedirectToAction("Error", "Course");
            }



        }
    }
}