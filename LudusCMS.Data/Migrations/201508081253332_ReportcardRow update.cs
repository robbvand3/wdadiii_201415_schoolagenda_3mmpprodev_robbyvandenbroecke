namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportcardRowupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportcardRows", "TaskPoint", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ReportcardRows", "TaskTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportcardRows", "TaskTotal");
            DropColumn("dbo.ReportcardRows", "TaskPoint");
        }
    }
}
