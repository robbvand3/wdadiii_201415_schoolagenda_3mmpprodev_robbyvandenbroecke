namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Persons2StartupProjectNameLudusCMSWeb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Persons", new[] { "DepartmentId" });
            AlterColumn("dbo.Persons", "DepartmentId", c => c.Short());
            CreateIndex("dbo.Persons", "DepartmentId");
            AddForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Persons", new[] { "DepartmentId" });
            AlterColumn("dbo.Persons", "DepartmentId", c => c.Short(nullable: false));
            CreateIndex("dbo.Persons", "DepartmentId");
            AddForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
        }
    }
}
