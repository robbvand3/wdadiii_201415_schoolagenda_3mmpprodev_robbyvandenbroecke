namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updateclass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classes", "DepartmentId", c => c.Short(nullable: false));
            CreateIndex("dbo.Classes", "DepartmentId");
            AddForeignKey("dbo.Classes", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classes", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Classes", new[] { "DepartmentId" });
            DropColumn("dbo.Classes", "DepartmentId");
        }
    }
}
