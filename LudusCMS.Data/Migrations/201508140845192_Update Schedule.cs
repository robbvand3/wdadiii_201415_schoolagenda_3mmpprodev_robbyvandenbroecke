namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateSchedule : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Schedules", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.ScheduleRows", "ScheduleId", "dbo.Schedules");
            DropIndex("dbo.ScheduleRows", new[] { "ScheduleId" });
            DropIndex("dbo.Schedules", new[] { "ClassId" });
            CreateTable(
                "dbo.ScheduleColumns",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        ClassId = c.Short(nullable: false),
                        ScheduleId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Schedules", t => t.ScheduleId, cascadeDelete: true)
                .Index(t => t.ClassId)
                .Index(t => t.ScheduleId);
            
            AddColumn("dbo.ScheduleRows", "Description", c => c.String(nullable: false, maxLength: 1024));
            AddColumn("dbo.ScheduleRows", "ScheduleColumnId", c => c.Short(nullable: false));
            CreateIndex("dbo.ScheduleRows", "ScheduleColumnId");
            AddForeignKey("dbo.ScheduleRows", "ScheduleColumnId", "dbo.ScheduleColumns", "Id", cascadeDelete: true);
            DropColumn("dbo.ScheduleRows", "ScheduleId");
            DropColumn("dbo.Schedules", "ClassId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Schedules", "ClassId", c => c.Short(nullable: false));
            AddColumn("dbo.ScheduleRows", "ScheduleId", c => c.Short(nullable: false));
            DropForeignKey("dbo.ScheduleRows", "ScheduleColumnId", "dbo.ScheduleColumns");
            DropForeignKey("dbo.ScheduleColumns", "ScheduleId", "dbo.Schedules");
            DropForeignKey("dbo.ScheduleColumns", "ClassId", "dbo.Classes");
            DropIndex("dbo.ScheduleColumns", new[] { "ScheduleId" });
            DropIndex("dbo.ScheduleColumns", new[] { "ClassId" });
            DropIndex("dbo.ScheduleRows", new[] { "ScheduleColumnId" });
            DropColumn("dbo.ScheduleRows", "ScheduleColumnId");
            DropColumn("dbo.ScheduleRows", "Description");
            DropTable("dbo.ScheduleColumns");
            CreateIndex("dbo.Schedules", "ClassId");
            CreateIndex("dbo.ScheduleRows", "ScheduleId");
            AddForeignKey("dbo.ScheduleRows", "ScheduleId", "dbo.Schedules", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Schedules", "ClassId", "dbo.Classes", "Id", cascadeDelete: true);
        }
    }
}
