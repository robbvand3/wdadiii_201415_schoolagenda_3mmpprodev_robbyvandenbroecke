namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updatemessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Message_Id", c => c.Short());
            CreateIndex("dbo.AspNetUsers", "Message_Id");
            AddForeignKey("dbo.AspNetUsers", "Message_Id", "dbo.Messages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Message_Id", "dbo.Messages");
            DropIndex("dbo.AspNetUsers", new[] { "Message_Id" });
            DropColumn("dbo.AspNetUsers", "Message_Id");
        }
    }
}
