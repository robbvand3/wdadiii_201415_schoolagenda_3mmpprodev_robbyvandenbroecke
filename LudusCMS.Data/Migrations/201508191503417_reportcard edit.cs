namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reportcardedit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Reportcard_Id", c => c.Short());
            CreateIndex("dbo.AspNetUsers", "Reportcard_Id");
            AddForeignKey("dbo.AspNetUsers", "Reportcard_Id", "dbo.Reportcards", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Reportcard_Id", "dbo.Reportcards");
            DropIndex("dbo.AspNetUsers", new[] { "Reportcard_Id" });
            DropColumn("dbo.AspNetUsers", "Reportcard_Id");
        }
    }
}
