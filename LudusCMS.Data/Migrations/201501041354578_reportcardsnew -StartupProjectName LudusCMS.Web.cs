namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reportcardsnewStartupProjectNameLudusCMSWeb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportcardRows", "Description", c => c.String(maxLength: 1024));
            AddColumn("dbo.Reportcards", "Title", c => c.String(nullable: false, maxLength: 256));
            AddColumn("dbo.Reportcards", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Reportcards", "UserId");
            AddForeignKey("dbo.Reportcards", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reportcards", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Reportcards", new[] { "UserId" });
            DropColumn("dbo.Reportcards", "UserId");
            DropColumn("dbo.Reportcards", "Title");
            DropColumn("dbo.ReportcardRows", "Description");
        }
    }
}
