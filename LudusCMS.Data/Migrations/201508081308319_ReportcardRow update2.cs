namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportcardRowupdate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportcardRows", "TaskScore", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ReportcardRows", "TestScore", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ReportcardRows", "TestPoint", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ReportcardRows", "TestTotal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ReportcardRows", "TotalScore", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ReportcardRows", "Score");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReportcardRows", "Score", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ReportcardRows", "TotalScore");
            DropColumn("dbo.ReportcardRows", "TestTotal");
            DropColumn("dbo.ReportcardRows", "TestPoint");
            DropColumn("dbo.ReportcardRows", "TestScore");
            DropColumn("dbo.ReportcardRows", "TaskScore");
        }
    }
}
