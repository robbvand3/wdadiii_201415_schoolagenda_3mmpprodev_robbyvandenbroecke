namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationUser2StartupProjectNameLudusCMSWeb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "PersonId", "dbo.Persons");
            DropIndex("dbo.AspNetUsers", new[] { "PersonId" });
            AddColumn("dbo.AspNetUsers", "RoleId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "RoleId");
            AddForeignKey("dbo.AspNetUsers", "RoleId", "dbo.AspNetRoles", "Id");
            DropColumn("dbo.AspNetUsers", "PersonId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "PersonId", c => c.Int());
            DropForeignKey("dbo.AspNetUsers", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "RoleId" });
            DropColumn("dbo.AspNetUsers", "RoleId");
            CreateIndex("dbo.AspNetUsers", "PersonId");
            AddForeignKey("dbo.AspNetUsers", "PersonId", "dbo.Persons", "Id");
        }
    }
}
