namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Gebruikers1StartupProjectNameLudusCMSweb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Persons", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.Persons", new[] { "RoleId" });
            DropColumn("dbo.Persons", "RoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Persons", "RoleId", c => c.Int(nullable: false));
            CreateIndex("dbo.Persons", "RoleId");
            AddForeignKey("dbo.Persons", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
    }
}
