namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test1MigratieStartUpProjectNameLudusCMSWeb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        Synopsis = c.String(nullable: false, maxLength: 1024),
                        Content = c.String(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        DepartmentId = c.Short(),
                        ApplicationUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.DepartmentId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Description = c.String(maxLength: 512),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        BlogId = c.Int(nullable: false),
                        ParentCommentId = c.Long(),
                        ApplicationUser_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blogs", t => t.BlogId, cascadeDelete: true)
                .ForeignKey("dbo.Comments", t => t.ParentCommentId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.BlogId)
                .Index(t => t.ParentCommentId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        SchoolYearId = c.Short(nullable: false),
                        CourseId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SchoolYears", t => t.SchoolYearId, cascadeDelete: true)
                .Index(t => t.SchoolYearId);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        DepartmentId = c.Short(nullable: false),
                        ClassId = c.Short(nullable: false),
                        ScheduleRowId = c.Short(nullable: false),
                        ReportcardRowId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        CourseId = c.Short(nullable: false),
                        ScheduleId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        ClassId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.ScheduleRows",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Position = c.Int(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        CourseId = c.Short(nullable: false),
                        ScheduleId = c.Short(nullable: false),
                        ClassroomId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.Schedules", t => t.ScheduleId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.ScheduleId);
            
            CreateTable(
                "dbo.Classrooms",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        ScheduleRowId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(nullable: false, maxLength: 1024),
                        Start_Date = c.DateTime(nullable: false),
                        End_Date = c.DateTime(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        EventCategoryId = c.Short(nullable: false),
                        CourseId = c.Short(),
                        SignedFather = c.Boolean(nullable: false),
                        SignedMother = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Courses", t => t.CourseId)
                .ForeignKey("dbo.EventCategories", t => t.EventCategoryId, cascadeDelete: true)
                .Index(t => t.EventCategoryId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.EventCategories",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportcardRows",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Score = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        CourseId = c.Short(),
                        ReportcardId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reportcards", t => t.ReportcardId, cascadeDelete: true)
                .Index(t => t.ReportcardId);
            
            CreateTable(
                "dbo.Reportcards",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        SignedFather = c.Boolean(nullable: false),
                        SignedMother = c.Boolean(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SchoolYears",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        SchoolYearStart = c.DateTime(nullable: false),
                        SchoolYearEnd = c.DateTime(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Created = c.Binary(),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.Binary(),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        PersonId = c.Int(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId)
                .Index(t => t.PersonId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 128),
                        DayOfBirth = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Blog_Has_Categories",
                c => new
                    {
                        BlogId = c.Int(nullable: false),
                        CategoryId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.BlogId, t.CategoryId })
                .ForeignKey("dbo.Blogs", t => t.BlogId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.BlogId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Class_Has_Courses",
                c => new
                    {
                        ClassId = c.Short(nullable: false),
                        CourseId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClassId, t.CourseId })
                .ForeignKey("dbo.Classes", t => t.ClassId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.ClassId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Classroom_Has_ScheduleRows",
                c => new
                    {
                        ClassroomId = c.Short(nullable: false),
                        ScheduleId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClassroomId, t.ScheduleId })
                .ForeignKey("dbo.Classrooms", t => t.ClassroomId, cascadeDelete: true)
                .ForeignKey("dbo.ScheduleRows", t => t.ScheduleId, cascadeDelete: true)
                .Index(t => t.ClassroomId)
                .Index(t => t.ScheduleId);
            
            CreateTable(
                "dbo.ReportcardRow_Has_Courses",
                c => new
                    {
                        ReportcardRowId = c.Short(nullable: false),
                        CourseId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.ReportcardRowId, t.CourseId })
                .ForeignKey("dbo.ReportcardRows", t => t.ReportcardRowId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.ReportcardRowId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Department_has_Courses",
                c => new
                    {
                        DepartmentId = c.Short(nullable: false),
                        CourseId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentId, t.CourseId })
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseId, cascadeDelete: true)
                .Index(t => t.DepartmentId)
                .Index(t => t.CourseId);
            
            CreateTable(
                "dbo.Blog_Has_Tags",
                c => new
                    {
                        BlogId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BlogId, t.TagId })
                .ForeignKey("dbo.Blogs", t => t.BlogId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.BlogId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        StudentNumber = c.String(),
                        StudentEmail = c.String(),
                        StudentPicture = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        LecturerNumber = c.String(),
                        LecturerEmail = c.String(),
                        LecturerPicture = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teachers", "Id", "dbo.Persons");
            DropForeignKey("dbo.Students", "Id", "dbo.Persons");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Blogs", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Blog_Has_Tags", "TagId", "dbo.Tags");
            DropForeignKey("dbo.Blog_Has_Tags", "BlogId", "dbo.Blogs");
            DropForeignKey("dbo.Blogs", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Departments", "SchoolYearId", "dbo.SchoolYears");
            DropForeignKey("dbo.Department_has_Courses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Department_has_Courses", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.ReportcardRows", "ReportcardId", "dbo.Reportcards");
            DropForeignKey("dbo.ReportcardRow_Has_Courses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.ReportcardRow_Has_Courses", "ReportcardRowId", "dbo.ReportcardRows");
            DropForeignKey("dbo.Events", "EventCategoryId", "dbo.EventCategories");
            DropForeignKey("dbo.Events", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.ScheduleRows", "ScheduleId", "dbo.Schedules");
            DropForeignKey("dbo.ScheduleRows", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Classroom_Has_ScheduleRows", "ScheduleId", "dbo.ScheduleRows");
            DropForeignKey("dbo.Classroom_Has_ScheduleRows", "ClassroomId", "dbo.Classrooms");
            DropForeignKey("dbo.Schedules", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Class_Has_Courses", "CourseId", "dbo.Courses");
            DropForeignKey("dbo.Class_Has_Courses", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Comments", "ParentCommentId", "dbo.Comments");
            DropForeignKey("dbo.Comments", "BlogId", "dbo.Blogs");
            DropForeignKey("dbo.Blog_Has_Categories", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Blog_Has_Categories", "BlogId", "dbo.Blogs");
            DropIndex("dbo.Teachers", new[] { "Id" });
            DropIndex("dbo.Students", new[] { "Id" });
            DropIndex("dbo.Blog_Has_Tags", new[] { "TagId" });
            DropIndex("dbo.Blog_Has_Tags", new[] { "BlogId" });
            DropIndex("dbo.Department_has_Courses", new[] { "CourseId" });
            DropIndex("dbo.Department_has_Courses", new[] { "DepartmentId" });
            DropIndex("dbo.ReportcardRow_Has_Courses", new[] { "CourseId" });
            DropIndex("dbo.ReportcardRow_Has_Courses", new[] { "ReportcardRowId" });
            DropIndex("dbo.Classroom_Has_ScheduleRows", new[] { "ScheduleId" });
            DropIndex("dbo.Classroom_Has_ScheduleRows", new[] { "ClassroomId" });
            DropIndex("dbo.Class_Has_Courses", new[] { "CourseId" });
            DropIndex("dbo.Class_Has_Courses", new[] { "ClassId" });
            DropIndex("dbo.Blog_Has_Categories", new[] { "CategoryId" });
            DropIndex("dbo.Blog_Has_Categories", new[] { "BlogId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "PersonId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ReportcardRows", new[] { "ReportcardId" });
            DropIndex("dbo.Events", new[] { "CourseId" });
            DropIndex("dbo.Events", new[] { "EventCategoryId" });
            DropIndex("dbo.ScheduleRows", new[] { "ScheduleId" });
            DropIndex("dbo.ScheduleRows", new[] { "CourseId" });
            DropIndex("dbo.Schedules", new[] { "ClassId" });
            DropIndex("dbo.Departments", new[] { "SchoolYearId" });
            DropIndex("dbo.Comments", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Comments", new[] { "ParentCommentId" });
            DropIndex("dbo.Comments", new[] { "BlogId" });
            DropIndex("dbo.Blogs", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Blogs", new[] { "DepartmentId" });
            DropTable("dbo.Teachers");
            DropTable("dbo.Students");
            DropTable("dbo.Blog_Has_Tags");
            DropTable("dbo.Department_has_Courses");
            DropTable("dbo.ReportcardRow_Has_Courses");
            DropTable("dbo.Classroom_Has_ScheduleRows");
            DropTable("dbo.Class_Has_Courses");
            DropTable("dbo.Blog_Has_Categories");
            DropTable("dbo.Persons");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Tags");
            DropTable("dbo.SchoolYears");
            DropTable("dbo.Reportcards");
            DropTable("dbo.ReportcardRows");
            DropTable("dbo.EventCategories");
            DropTable("dbo.Events");
            DropTable("dbo.Classrooms");
            DropTable("dbo.ScheduleRows");
            DropTable("dbo.Schedules");
            DropTable("dbo.Classes");
            DropTable("dbo.Courses");
            DropTable("dbo.Departments");
            DropTable("dbo.Comments");
            DropTable("dbo.Categories");
            DropTable("dbo.Blogs");
        }
    }
}
