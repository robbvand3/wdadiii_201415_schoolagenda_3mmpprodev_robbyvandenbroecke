namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Teachertocourse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classes", "Teacher_Id", c => c.Int());
            AddColumn("dbo.Courses", "TeacherId", c => c.Int(nullable: false));
            CreateIndex("dbo.Classes", "Teacher_Id");
            CreateIndex("dbo.Courses", "TeacherId");
            AddForeignKey("dbo.Courses", "TeacherId", "dbo.AspNetUsers", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Classes", "Teacher_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Courses", "ScheduleRowId");
            DropColumn("dbo.Courses", "ReportcardRowId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "ReportcardRowId", c => c.Short(nullable: false));
            AddColumn("dbo.Courses", "ScheduleRowId", c => c.Short(nullable: false));
            DropForeignKey("dbo.Classes", "Teacher_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Courses", "TeacherId", "dbo.AspNetUsers");
            DropIndex("dbo.Courses", new[] { "TeacherId" });
            DropIndex("dbo.Classes", new[] { "Teacher_Id" });
            DropColumn("dbo.Courses", "TeacherId");
            DropColumn("dbo.Classes", "Teacher_Id");
        }
    }
}
