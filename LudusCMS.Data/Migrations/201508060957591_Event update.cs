namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eventupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "DepartmentId", c => c.Short());
            AddColumn("dbo.Events", "ClassId", c => c.Short());
            CreateIndex("dbo.Events", "DepartmentId");
            CreateIndex("dbo.Events", "ClassId");
            AddForeignKey("dbo.Events", "ClassId", "dbo.Classes", "Id");
            AddForeignKey("dbo.Events", "DepartmentId", "dbo.Departments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Events", "ClassId", "dbo.Classes");
            DropIndex("dbo.Events", new[] { "ClassId" });
            DropIndex("dbo.Events", new[] { "DepartmentId" });
            DropColumn("dbo.Events", "ClassId");
            DropColumn("dbo.Events", "DepartmentId");
        }
    }
}
