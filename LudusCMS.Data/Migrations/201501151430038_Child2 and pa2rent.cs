namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Child2andpa2rent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ClassId", c => c.Short());
            AddColumn("dbo.AspNetUsers", "ChildId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "ClassId");
            CreateIndex("dbo.AspNetUsers", "ChildId");
            AddForeignKey("dbo.AspNetUsers", "ChildId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUsers", "ClassId", "dbo.Classes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.AspNetUsers", "ChildId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsers", new[] { "ChildId" });
            DropIndex("dbo.AspNetUsers", new[] { "ClassId" });
            DropColumn("dbo.AspNetUsers", "ChildId");
            DropColumn("dbo.AspNetUsers", "ClassId");
        }
    }
}
