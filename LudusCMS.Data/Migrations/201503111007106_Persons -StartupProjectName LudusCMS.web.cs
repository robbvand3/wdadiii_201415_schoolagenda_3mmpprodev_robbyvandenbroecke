namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersonsStartupProjectNameLudusCMSweb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Persons", "RoleId", c => c.Int(nullable: false));
            AddColumn("dbo.Persons", "DepartmentId", c => c.Short(nullable: false));
            CreateIndex("dbo.Persons", "RoleId");
            CreateIndex("dbo.Persons", "DepartmentId");
            AddForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Persons", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Persons", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Persons", new[] { "DepartmentId" });
            DropIndex("dbo.Persons", new[] { "RoleId" });
            DropColumn("dbo.Persons", "DepartmentId");
            DropColumn("dbo.Persons", "RoleId");
        }
    }
}
