namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class signatureparent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reportcards", "Signature1", c => c.Int());
            AddColumn("dbo.Reportcards", "Signature2", c => c.Int());
            DropColumn("dbo.Reportcards", "SignedFather");
            DropColumn("dbo.Reportcards", "SignedMother");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reportcards", "SignedMother", c => c.Boolean(nullable: false));
            AddColumn("dbo.Reportcards", "SignedFather", c => c.Boolean(nullable: false));
            DropColumn("dbo.Reportcards", "Signature2");
            DropColumn("dbo.Reportcards", "Signature1");
        }
    }
}
