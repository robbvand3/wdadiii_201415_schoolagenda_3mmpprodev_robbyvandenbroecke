namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class optionaldescription : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ScheduleRows", "Description", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ScheduleRows", "Description", c => c.String(nullable: false, maxLength: 1024));
        }
    }
}
