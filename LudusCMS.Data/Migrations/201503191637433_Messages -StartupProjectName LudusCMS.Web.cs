namespace LudusCMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessagesStartupProjectNameLudusCMSWeb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        Subject = c.String(),
                        Content = c.String(nullable: false),
                        Created = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Updated = c.DateTime(),
                        Deleted = c.DateTime(),
                        SenderId = c.Int(nullable: false),
                        ReceiverId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ReceiverId, cascadeDelete: true)
                .Index(t => t.ReceiverId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "ReceiverId", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "ReceiverId" });
            DropTable("dbo.Messages");
        }
    }
}
