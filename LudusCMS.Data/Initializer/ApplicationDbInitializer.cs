﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LudusCMS.Data.Persistence;
using LudusCMS.Model;

namespace LudusCMS.Data.Initializer
{
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializePosts(context);

            base.Seed(context);
        }
        public override void InitializeDatabase(ApplicationDbContext context)
        {
            base.InitializeDatabase(context);
        }

        #region Private Methods
        private void InitializePosts(ApplicationDbContext context)
        {
            Blog blog = null;
            for (var i = 0; i < 50; i++)
            {
                blog = new Blog
                {
                    Title = "Title of post " + i,
                    Synopsis = "Tumblr before they sold out artisan locavore kale chips, leggings pour-over Vice banh mi 90's trust fund 8-bit skateboard Blue Bottle. Cred +1 retro tofu master cleanse pug squid American Apparel pickled Pitchfork, Godard whatever organic YOLO. Flexitarian leggings selvage wayfarers. Gluten-free Tumblr kitsch mlkshk gastropub, meh try-hard chillwave sriracha Wes Anderson roof party food truck. Literally authentic Blue Bottle hella tousled irony. Pop-up organic street art master cleanse, literally ugh actually farm-to-table lomo PBR&B Brooklyn viral kitsch. Farm-to-table High Life Bushwick, literally letterpress umami gentrify Tonx Kickstarter asymmetrical blog.",
                    Content = "<p>Tumblr before they sold out artisan locavore kale chips, leggings pour-over Vice banh mi 90's trust fund 8-bit skateboard Blue Bottle. Cred +1 retro tofu master cleanse pug squid American Apparel pickled Pitchfork, Godard whatever organic YOLO. Flexitarian leggings selvage wayfarers. Gluten-free Tumblr kitsch mlkshk gastropub, meh try-hard chillwave sriracha Wes Anderson roof party food truck. Literally authentic Blue Bottle hella tousled irony. Pop-up organic street art master cleanse, literally ugh actually farm-to-table lomo PBR&B Brooklyn viral kitsch. Farm-to-table High Life Bushwick, literally letterpress umami gentrify Tonx Kickstarter asymmetrical blog.</p><p>Shabby chic normcore gluten-free whatever forage leggings authentic cornhole food truck Banksy, kogi four loko slow-carb Tonx. Jean shorts viral readymade, skateboard Truffaut authentic blog chillwave 8-bit umami. Four loko Banksy whatever, aesthetic paleo meh DIY gluten-free. Blog Truffaut tilde irony, Schlitz authentic Shoreditch Intelligentsia lo-fi semiotics tote bag narwhal raw denim freegan. Lomo Helvetica semiotics fap. Blue Bottle sartorial keytar brunch, leggings salvia McSweeney's. Artisan actually photo booth, Pitchfork yr butcher fanny pack Etsy beard post-ironic gastropub vegan.</p><p>Organic aesthetic fap, farm-to-table bitters cred seitan photo booth yr cliche. Helvetica synth +1, you probably haven't heard of them Vice biodiesel viral ugh scenester vegan. Salvia VHS messenger bag Marfa, mumblecore retro vinyl fashion axe High Life dreamcatcher deep v. 3 wolf moon farm-to-table post-ironic, occupy kale chips Marfa synth bespoke biodiesel. Ethical gentrify meh blog, flexitarian kogi mixtape brunch Truffaut 90's Tonx authentic. Polaroid you probably haven't heard of them bicycle rights, irony butcher meh pour-over banh mi Pinterest aesthetic. Vinyl mlkshk bicycle rights, fashion axe shabby chic irony tattooed raw denim roof party.</p>"
                };

                context.Blogs.Add(blog);
            }
            context.SaveChanges();
        }
        #endregion
    }
}
