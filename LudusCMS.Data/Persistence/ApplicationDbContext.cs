﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LudusCMS.Data.Initializer;
using LudusCMS.Model;

namespace LudusCMS.Data.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        #region Properties
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<Blog> Blogs { get; set; }
        public virtual IDbSet<Comment> Comments { get; set; }
        public virtual IDbSet<SchoolYear> SchoolYears { get; set; }
        public virtual IDbSet<Department> Departments { get; set; }
        #endregion

        #region Constructor
        public ApplicationDbContext() : base("DefaultConnection") { this.Configuration.LazyLoadingEnabled = false; }
        static ApplicationDbContext()
        {
            //Seeds the database
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        #endregion

        #region Protected Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if(modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder");
            }
            base.OnModelCreating(modelBuilder);

            #region Categories
            EntityTypeConfiguration<Category> efConfCategory = modelBuilder.Entity<Category>().ToTable("Categories");
            efConfCategory.HasKey((Category m) => m.Id);//Linq notation
            efConfCategory.Property((Category m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfCategory.Property((Category m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfCategory.Property((Category m) => m.Name).IsRequired().HasMaxLength(64);
            efConfCategory.Property((Category m) => m.Description).IsOptional().HasMaxLength(512);
            efConfCategory.Property((Category m) => m.Updated).IsOptional();
            efConfCategory.Property((Category m) => m.Deleted).IsOptional();
            #endregion

            #region Blogs
            EntityTypeConfiguration<Blog> efConfBlog = modelBuilder.Entity<Blog>().ToTable("Blogs");
            efConfBlog.HasKey((Blog m) => m.Id);//Linq notation
            efConfBlog.Property((Blog m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfBlog.Property((Blog m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfBlog.Property((Blog m) => m.Title).IsRequired().HasMaxLength(256);
            efConfBlog.Property((Blog m) => m.Synopsis).IsRequired().HasMaxLength(1024);
            efConfBlog.Property((Blog m) => m.Content).IsRequired();
            efConfBlog.Property((Blog m) => m.Updated).IsOptional();
            efConfBlog.Property((Blog m) => m.Deleted).IsOptional();
            efConfBlog
                .HasMany((Blog m) => m.Categories)
                .WithMany((Category c) => c.Blogs)
                .Map(mc =>
                {
                    mc.ToTable("Blog_Has_Categories");
                    mc.MapLeftKey("BlogId");
                    mc.MapRightKey("CategoryId");
                });
            efConfBlog
                .HasMany((Blog m) => m.Tags)
                .WithMany((Tag t) => t.Blogs)
                .Map(mc =>
                {
                    mc.ToTable("Blog_Has_Tags");
                    mc.MapLeftKey("BlogId");
                    mc.MapRightKey("TagId");
                });
            efConfBlog.HasOptional((Blog p) => p.Department).WithMany((Department d) => d.Blogs).HasForeignKey((Blog f) => f.DepartmentId);
            #endregion

            #region Tag
            EntityTypeConfiguration<Tag> efConfTag = modelBuilder.Entity<Tag>().ToTable("Tags");
            efConfTag.HasKey((Tag m) => m.Id);//Linq notation
            efConfTag.Property((Tag m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfTag.Property((Tag m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfTag.Property((Tag m) => m.Name).IsRequired().HasMaxLength(64);
            efConfTag.Property((Tag m) => m.Updated).IsOptional();
            efConfTag.Property((Tag m) => m.Deleted).IsOptional();
            #endregion

            #region Comment
            EntityTypeConfiguration<Comment> efConfComment = modelBuilder.Entity<Comment>().ToTable("Comments");
            efConfComment.HasKey((Comment m) => m.Id);//Linq notation
            efConfComment.Property((Comment m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfComment.Property((Comment m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfComment.Property((Comment m) => m.Content).IsRequired().HasMaxLength(1024);
            efConfComment.Property((Comment m) => m.Updated).IsOptional();
            efConfComment.Property((Comment m) => m.Deleted).IsOptional();
            efConfComment.HasRequired((Comment m) => m.Blog).WithMany((Blog p) => p.Comments).HasForeignKey((Comment z) => z.BlogId);
            efConfComment.HasOptional((Comment m) => m.ParentComment).WithMany((Comment p) => p.ChildComments).HasForeignKey((Comment z) => z.ParentCommentId);
            efConfComment.HasOptional((Comment m) => m.User).WithMany((ApplicationUser u) => u.Comments).HasForeignKey((Comment f) => f.UserId);
            #endregion

            #region SchoolYear
            EntityTypeConfiguration<SchoolYear> efConfSchoolYear = modelBuilder.Entity<SchoolYear>().ToTable("SchoolYears");
            efConfSchoolYear.HasKey((SchoolYear m) => m.Id);//Linq notation
            efConfSchoolYear.Property((SchoolYear m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfSchoolYear.Property((SchoolYear m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfSchoolYear.Property((SchoolYear m) => m.Name).IsRequired().HasMaxLength(128);
            efConfSchoolYear.Property((SchoolYear m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfSchoolYear.Property((SchoolYear m) => m.SchoolYearStart).IsRequired();
            efConfSchoolYear.Property((SchoolYear m) => m.SchoolYearEnd).IsRequired();
            efConfSchoolYear.Property((SchoolYear m) => m.Updated).IsOptional();
            efConfSchoolYear.Property((SchoolYear m) => m.Deleted).IsOptional();
            #endregion 

            #region Department
            EntityTypeConfiguration<Department> efConfDepartment = modelBuilder.Entity<Department>().ToTable("Departments");
            efConfDepartment.HasKey((Department m) => m.Id);//Linq notation
            efConfDepartment.Property((Department m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfDepartment.Property((Department m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfDepartment.Property((Department m) => m.Name).IsRequired().HasMaxLength(128);
            efConfDepartment.Property((Department m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfDepartment.Property((Department m) => m.Updated).IsOptional();
            efConfDepartment.Property((Department m) => m.Deleted).IsOptional();
            efConfDepartment.HasRequired((Department m) => m.SchoolYear).WithMany((SchoolYear a) => a.Departments).HasForeignKey((Department f) => f.SchoolYearId);
            efConfDepartment
                .HasMany((Department m) => m.Courses)
                .WithMany((Course c) => c.Departments)
                .Map(mc =>
                {
                    mc.ToTable("Department_has_Courses");
                    mc.MapLeftKey("DepartmentId");
                    mc.MapRightKey("CourseId");
                });
            #endregion

            #region Course
            EntityTypeConfiguration<Course> efConfCourse = modelBuilder.Entity<Course>().ToTable("Courses");
            efConfCourse.HasKey((Course m) => m.Id);//Linq notation;
            efConfCourse.Property((Course m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfCourse.Property((Course m) => m.Name).IsRequired().HasMaxLength(128);
            efConfCourse.Property((Course m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfCourse.Property((Course m) => m.Updated).IsOptional();
            efConfCourse.Property((Course m) => m.Deleted).IsOptional();
            efConfCourse.HasRequired((Course m) => m.Teacher).WithMany((ApplicationUser a) => a.Courses).HasForeignKey((Course f) => f.TeacherId);
       
            #endregion

            #region Class
            EntityTypeConfiguration<Class> efConfClass = modelBuilder.Entity<Class>().ToTable("Classes");
            efConfClass.HasKey((Class m) => m.Id);//Linq notation;
            efConfClass.Property((Class m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfClass.Property((Class m) => m.Name).IsRequired().HasMaxLength(128);
            efConfClass.Property((Class m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfClass.Property((Class m) => m.Updated).IsOptional();
            efConfClass.Property((Class m) => m.Deleted).IsOptional();
            efConfClass.HasRequired((Class m) => m.Department).WithMany((Department a) => a.Classes).HasForeignKey((Class f) => f.DepartmentId);
           
            efConfClass
               .HasMany((Class m) => m.Courses)
               .WithMany((Course c) => c.Classes)
               .Map(mc =>
               {
                   mc.ToTable("Class_Has_Courses");
                   mc.MapLeftKey("ClassId");
                   mc.MapRightKey("CourseId");
               });
            #endregion

            #region Schedule
            EntityTypeConfiguration<Schedule> efConfSchedule = modelBuilder.Entity<Schedule>().ToTable("Schedules");
            efConfSchedule.HasKey((Schedule m) => m.Id);//Linq notation;
            efConfSchedule.Property((Schedule m) => m.Name).IsRequired().HasMaxLength(128);
            efConfSchedule.Property((Schedule m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfSchedule.Property((Schedule m) => m.Updated).IsOptional();
            efConfSchedule.Property((Schedule m) => m.Deleted).IsOptional();
           
            #endregion

            #region ScheduleColumn
            EntityTypeConfiguration<ScheduleColumn> efConfScheduleColumn = modelBuilder.Entity<ScheduleColumn>().ToTable("ScheduleColumns");
            efConfScheduleColumn.HasKey((ScheduleColumn m) => m.Id);//Linq notation;
            efConfScheduleColumn.Property((ScheduleColumn m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfScheduleColumn.Property((ScheduleColumn m) => m.Updated).IsOptional();
            efConfScheduleColumn.Property((ScheduleColumn m) => m.Deleted).IsOptional();
            efConfScheduleColumn.HasRequired((ScheduleColumn m) => m.Class).WithMany((Class a) => a.ScheduleColumns).HasForeignKey((ScheduleColumn f) => f.ClassId);
            efConfScheduleColumn.HasRequired((ScheduleColumn m) => m.Schedule).WithMany((Schedule a) => a.ScheduleColumns).HasForeignKey((ScheduleColumn f) => f.ScheduleId);
            #endregion

            #region ScheduleRow
            EntityTypeConfiguration<ScheduleRow> efConfScheduleRow = modelBuilder.Entity<ScheduleRow>().ToTable("ScheduleRows");
            efConfScheduleRow.HasKey((ScheduleRow m) => m.Id);//Linq notation;
            efConfScheduleRow.Property((ScheduleRow m) => m.Description).IsOptional().HasMaxLength(1024);
            efConfScheduleRow.Property((ScheduleRow m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfScheduleRow.Property((ScheduleRow m) => m.Position).IsRequired();
            efConfScheduleRow.Property((ScheduleRow m) => m.Updated).IsOptional();
            efConfScheduleRow.Property((ScheduleRow m) => m.Deleted).IsOptional();
            efConfScheduleRow.HasRequired((ScheduleRow m) => m.Course).WithMany((Course a) => a.ScheduleRows).HasForeignKey((ScheduleRow f) => f.CourseId);
            efConfScheduleRow.HasRequired((ScheduleRow m) => m.ScheduleColumn).WithMany((ScheduleColumn a) => a.ScheduleRows).HasForeignKey((ScheduleRow f) => f.ScheduleColumnId);
            #endregion

            #region Classroom
            EntityTypeConfiguration<Classroom> efConfClassroom = modelBuilder.Entity<Classroom>().ToTable("Classrooms");
            efConfClassroom.HasKey((Classroom m) => m.Id);//Linq notation;
            efConfClassroom.Property((Classroom m) => m.Name).IsRequired().HasMaxLength(128);
            efConfClassroom.Property((Classroom m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfClassroom.Property((Classroom m) => m.Updated).IsOptional();
            efConfClassroom.Property((Classroom m) => m.Deleted).IsOptional();
            efConfClassroom
               .HasMany((Classroom m) => m.ScheduleRows)
               .WithMany((ScheduleRow c) => c.Classrooms)
               .Map(mc =>
               {
                   mc.ToTable("Classroom_Has_ScheduleRows");
                   mc.MapLeftKey("ClassroomId");
                   mc.MapRightKey("ScheduleId");
               });
            #endregion

            #region Reportcard
            EntityTypeConfiguration<Reportcard> efConfReportcard = modelBuilder.Entity<Reportcard>().ToTable("Reportcards");
            efConfReportcard.HasKey((Reportcard m) => m.Id);//Linq notation;
            efConfReportcard.Property((Reportcard m) => m.Signature1).IsOptional();
            efConfReportcard.Property((Reportcard m) => m.Signature2).IsOptional();
            efConfReportcard.Property((Reportcard m) => m.Title).IsRequired().HasMaxLength(256);
            efConfReportcard.Property((Reportcard m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfReportcard.Property((Reportcard m) => m.Updated).IsOptional();
            efConfReportcard.Property((Reportcard m) => m.Deleted).IsOptional();
            efConfReportcard.HasRequired((Reportcard m) => m.User).WithMany((ApplicationUser u) => u.Reportcards).HasForeignKey((Reportcard f) => f.UserId);
            #endregion

            #region ReportcardRow
            EntityTypeConfiguration<ReportcardRow> efConfReportcardRow = modelBuilder.Entity<ReportcardRow>().ToTable("ReportcardRows");
            efConfReportcardRow.HasKey((ReportcardRow m) => m.Id);//Linq notation;
            efConfReportcardRow.Property((ReportcardRow m) => m.TaskScore).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TaskPoint).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TaskTotal).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TestScore).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TestPoint).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TestTotal).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.TotalScore).IsRequired();
            efConfReportcardRow.Property((ReportcardRow m) => m.Description).IsOptional().HasMaxLength(1024);
            efConfReportcardRow.Property((ReportcardRow m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfReportcardRow.Property((ReportcardRow m) => m.Updated).IsOptional();
            efConfReportcardRow.Property((ReportcardRow m) => m.Deleted).IsOptional();
            efConfReportcardRow.HasRequired((ReportcardRow m) => m.Reportcard).WithMany((Reportcard a) => a.ReportcardRows).HasForeignKey((ReportcardRow f) => f.ReportcardId);
            efConfReportcardRow
              .HasMany((ReportcardRow m) => m.Courses)
              .WithMany((Course c) => c.ReportcardRows)
              .Map(mc =>
              {
                  mc.ToTable("ReportcardRow_Has_Courses");
                  mc.MapLeftKey("ReportcardRowId");
                  mc.MapRightKey("CourseId");
              });
            #endregion

            #region Event
            EntityTypeConfiguration<Event> efConfEvent = modelBuilder.Entity<Event>().ToTable("Events");
            efConfEvent.HasKey((Event m) => m.Id);//Linq notation;
            efConfEvent.Property((Event m) => m.Name).IsRequired().HasMaxLength(256);
            efConfEvent.Property((Event m) => m.Description).IsRequired().HasMaxLength(1024);
            efConfEvent.Property((Event m) => m.Start_Date).IsRequired();
            efConfEvent.Property((Event m) => m.End_Date).IsRequired();
            efConfEvent.Property((Event m) => m.SignedFather).IsRequired();
            efConfEvent.Property((Event m) => m.SignedMother).IsRequired();
            efConfEvent.Property((Event m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfEvent.Property((Event m) => m.Updated).IsOptional();
            efConfEvent.Property((Event m) => m.Deleted).IsOptional();
            efConfEvent.HasRequired((Event m) => m.EventCategory).WithMany((EventCategory a) => a.Events).HasForeignKey((Event f) => f.EventCategoryId);
            efConfEvent.HasOptional((Event m) => m.Course).WithMany((Course a) => a.Events).HasForeignKey((Event f) => f.CourseId);
            efConfEvent.HasOptional((Event m) => m.Class).WithMany((Class a) => a.Events).HasForeignKey((Event f) => f.ClassId);
            efConfEvent.HasOptional((Event m) => m.Department).WithMany((Department a) => a.Events).HasForeignKey((Event f) => f.DepartmentId);
            #endregion 

            #region EventCategory
            EntityTypeConfiguration<EventCategory> efConfEventCategory = modelBuilder.Entity<EventCategory>().ToTable("EventCategories");
            efConfEventCategory.HasKey((EventCategory m) => m.Id);//Linq notation;
            efConfEventCategory.Property((EventCategory m) => m.Name).IsRequired().HasMaxLength(128);
            efConfEventCategory.Property((EventCategory m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfEventCategory.Property((EventCategory m) => m.Updated).IsOptional();
            efConfEventCategory.Property((EventCategory m) => m.Deleted).IsOptional();
            #endregion

            #region Persons 
            // Map Person Model to the Persons Table
            EntityTypeConfiguration<Person> efConfPerson = modelBuilder.Entity<Person>().ToTable("Persons");
            efConfPerson.HasKey((Person m) => m.Id);
            efConfPerson.Property((Person m) => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            efConfPerson.Property((Person m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfPerson.Property((Person m) => m.FirstName).IsRequired().HasMaxLength(64);
            efConfPerson.Property((Person m) => m.LastName).IsRequired().HasMaxLength(128);
            efConfPerson.Property((Person m) => m.DayOfBirth).IsOptional();
            efConfPerson.Property((Person m) => m.Email).IsOptional().HasMaxLength(256);
           
            efConfPerson.HasOptional((Person m) => m.Department).WithMany((Department a) => a.Persons).HasForeignKey((Person f) => f.DepartmentId);


            modelBuilder.Entity<ApplicationUser>().Property((ApplicationUser m) => m.FirstName).IsOptional().HasMaxLength(128);
            modelBuilder.Entity<ApplicationUser>().Property((ApplicationUser m) => m.LastName).IsOptional().HasMaxLength(128);
            modelBuilder.Entity<ApplicationUser>().HasOptional((ApplicationUser m) => m.Class).WithMany((Class c) => c.Students).HasForeignKey((ApplicationUser f) => f.ClassId);
           
            modelBuilder.Entity<ApplicationUser>().HasOptional((ApplicationUser m) => m.Department).WithMany((Department c) => c.Person).HasForeignKey((ApplicationUser f) => f.DepartmentId);
            modelBuilder.Entity<ApplicationUser>().HasOptional((ApplicationUser m) => m.Child).WithMany((ApplicationUser c) => c.Parents).HasForeignKey((ApplicationUser f) => f.ChildId);
            #endregion

            #region Students 
            // Map Student Model to the Students Table
            modelBuilder.Entity<Student>().ToTable("Students");
            #endregion 

            #region Teacher
            // Map Lecturer Model to the Lecturers Table
            modelBuilder.Entity<Teacher>().ToTable("Teachers");
            #endregion 

            #region Messages 
            EntityTypeConfiguration<Message> efConfMessage = modelBuilder.Entity<Message>().ToTable("Messages");
            efConfMessage.HasKey((Message m) => m.Id);//Linq notation;
            efConfMessage.Property((Message m) => m.Title).IsRequired().HasMaxLength(256);
            efConfMessage.Property((Message m) => m.Subject).IsOptional();
            efConfMessage.Property((Message m) => m.Content).IsRequired();
            efConfMessage.Property((Message m) => m.Created).IsRowVersion().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            efConfMessage.Property((Message m) => m.Updated).IsOptional();
            efConfMessage.Property((Message m) => m.Deleted).IsOptional();
            efConfMessage.HasRequired((Message m) => m.User).WithMany((ApplicationUser a) => a.Messages).HasForeignKey((Message f) => f.SenderId);
            efConfMessage.HasRequired((Message m) => m.User).WithMany((ApplicationUser a) => a.Messages).HasForeignKey((Message f) => f.ReceiverId);
            
            #endregion
        }
        #endregion
        #region Public Methods
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
        #endregion
    }   
}