﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LudusCMS.Data.Persistence;
using LudusCMS.Model;
using Data.Pattern.UnitOfWork;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Security.Policy;

namespace LudusCMS.Data.Test
{
    [TestClass] public class PersistanceTest
    {
        #region Variables
        GenericUnitOfWork _unitOfWork = new GenericUnitOfWork(new ApplicationDbContext());
        #endregion

        [TestMethod]
        public void UOW_Create_Post()
        {
            var post = new Blog
            {
                Title = "Facebook past authenticatiesysteem aan om pseudoniemen toe te staan",
                Synopsis = "Facebook heeft zijn verontschuldigingen aangeboden aan travestieten die niet met hun pseudoniemen actief mochten zijn, maar hun geboortenaam moesten gebruiken. De site gaat het authenticatiesysteem aanpassen voor pseudoniemen die leden in het dagelijks leven gebruiken.",
                Content = "<p>Facebook kwam onlangs onder vuur te liggen van travestieten en transseksuelen over zijn naamsbeleid: de leden werden er op aangesproken dat ze hun echte naam niet gebruikten en daarmee in strijd met de regels op Facebook handelden. Het gebruik van echte namen is de basis van Facebook: dit stelt de site in staat persoonlijker profielen op te stellen, wat waardevol is voor adverteerders.</p>"
            };

            _unitOfWork.Repository<Blog>().Insert(post);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
        [TestMethod]
        public void UOW_Create_Schoolyears()
        {
            for (int i = 5; i < 8; i++)
            {
                string startdate = "201" + i + "";
                int startdateint = Convert.ToInt32(startdate);
                string end = "201" + (i + 1) + "";
                int enddateint = Convert.ToInt32(end);
                var schoolyear = new SchoolYear
                {
                    Name = "" + startdate + "-" + end + "",
                    Description = "Het school jaar van " + startdate + "-" + end + ".",
                    SchoolYearEnd = new DateTime(enddateint, 7, 30),
                    SchoolYearStart = new DateTime(startdateint, 9, 1),
                };

                _unitOfWork.Repository<SchoolYear>().Insert(schoolyear);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }

        [TestMethod]
        public void UOW_Create_Schoolyear1()
        {
           var schoolyear = new SchoolYear
           {
              Name = "2014-2015",
              Description = "Het school jaar van 2014 - 2015",
           };

           _unitOfWork.Repository<SchoolYear>().Insert(schoolyear);
           int result = _unitOfWork.SaveChanges();

           Assert.IsNotNull(result);
           Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Department()
        {
            var schoolyear = _unitOfWork.Repository<SchoolYear>().GetById(12);
            Int16 id = schoolyear.Id;
            Assert.IsNotNull(schoolyear);

            var department = new Department
            {
                Name = "GDM",
                Description = "Grafische & Digitale Media",
                SchoolYearId = id
            };

            _unitOfWork.Repository<Department>().Insert(department);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Course()
        {
            var department = _unitOfWork.Repository<Department>().GetById(1);
            Int16 id = department.Id;
            Assert.IsNotNull(department);


            var course = new Course
            {
                Name = "NMDAD",
                Description = "New Media Design and Development",
                DepartmentId = id
            };

            _unitOfWork.Repository<Course>().Insert(course);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Class()
        {
            var course = _unitOfWork.Repository<Course>().GetById(1);
            Int16 id = course.Id;
            Assert.IsNotNull(course);
            for (int i = 1; i < 6; i++)
            {
                var classModel = new Class
                {
                    Name = "leerjaar" + i + "",
                    Description = "Beschrijving van class" + i + "",
                    CourseId = id
                };


                _unitOfWork.Repository<Class>().Insert(classModel);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }

        [TestMethod]
        public void UOW_Create_Schedule()
        {
            var classModel = _unitOfWork.Repository<Class>().GetById(1);
            Assert.IsNotNull(classModel);
            Int16 id = classModel.Id;

            var schedule = new ScheduleColumn
            {
                
                ClassId = id
            };

            _unitOfWork.Repository<ScheduleColumn>().Insert(schedule);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_ScheduleRow()
        {
            var course = _unitOfWork.Repository<Course>().GetById(1);
            Assert.IsNotNull(course);
            Int16 course_id = course.Id;

            var schedule = _unitOfWork.Repository<ScheduleColumn>().GetById(1);
            Assert.IsNotNull(schedule);
            Int16 schedule_id = schedule.Id;

            var classroom = _unitOfWork.Repository<Classroom>().GetById(1);
            Assert.IsNotNull(classroom);
            Int16 classroom_id = classroom.Id;

            var schedulerow = new ScheduleRow
            {
                Position = 1,
                CourseId = course_id,
                ScheduleColumnId = schedule_id,
                ClassroomId = classroom_id
            };

            _unitOfWork.Repository<ScheduleRow>().Insert(schedulerow);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Classroom()
        {
            var classroom = new Classroom
            {
                Name = "A12"
            };

            _unitOfWork.Repository<Classroom>().Insert(classroom);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Reportcard()
        {
            var reportcard = new Reportcard
            {
                
            };

            _unitOfWork.Repository<Reportcard>().Insert(reportcard);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_ReportcardRow()
        {
            var course = _unitOfWork.Repository<Course>().GetById(1);
            Assert.IsNotNull(course);
            Int16 course_id = course.Id;

            var reportcard = _unitOfWork.Repository<Reportcard>().GetById(1);
            Assert.IsNotNull(reportcard);
            Int16 reportcard_id = reportcard.Id;

            var reportcardrow = new ReportcardRow
            {
                TaskPoint = 14,
                TaskTotal = 20,
                TaskScore = 70,
                TestPoint = 14,
                TestTotal = 20,
                TestScore = 70,
                TotalScore = 70,
                CourseId = course_id,
                ReportcardId = reportcard_id

            };

            _unitOfWork.Repository<ReportcardRow>().Insert(reportcardrow);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_EventCategory()
        {
            var eventcategory = new EventCategory
            {
                Name = "Vakantie"
            };

              _unitOfWork.Repository<EventCategory>().Insert(eventcategory);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Event()
        {
            var eventcategory = _unitOfWork.Repository<EventCategory>().GetById(1);
            Assert.IsNotNull(eventcategory);
            Int16 eventcategory_id = eventcategory.Id;

            var evenement = new Event
            {
                Name = "Kerstvakantie",
                Description = "Feesten in de Kerst met een boom een vele pakjes!",
                Start_Date = new DateTime(2014, 12, 20),
                End_Date = new DateTime(2015, 1, 5),
                EventCategoryId = eventcategory_id
            };

            _unitOfWork.Repository<Event>().Insert(evenement);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);

        }

        [TestMethod]
        public void UOW_Create_Categories()
        {
            for (int i = 1; i < 11; i++)
            {
                var category = new Category
                {
                    Name = Convert.ToString(i),
                    Description = "Beschrijving van categorie " + i + ""
                };

                _unitOfWork.Repository<Category>().Insert(category);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }

        [TestMethod]
        public void UOW_Create_Person()
        {
            var person = new Person
            {
                FirstName ="Florian",
                LastName = "Van op den Dam",
                DayOfBirth = new DateTime(1993, 01, 11),
                Email = "Swaggy@hotmail.com"
            };

            _unitOfWork.Repository<Person>().Insert(person);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);

        }

        [TestMethod]
        public void UOW_Create_Student()
        {
            var student = new Student
            {
                StudentEmail = "vandammeflorian@school.be",
                StudentNumber = "0696969",
                StudentPicture = "http://johancutych.com/img/full/swag.png",
                FirstName = "Florian",
                LastName = "Van Damme",
                DayOfBirth = new DateTime(1993, 01, 11),
                Email = "Swaggerke@hotmail.com"
            };

            _unitOfWork.Repository<Student>().Insert(student);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

        [TestMethod]
        public void UOW_Create_Teacher()
        {
            var teacher = new Teacher
            {
                FirstName = "Robby",
                LastName = "van den Broecke",
                DayOfBirth = new DateTime(1994, 01, 14),
                Email = "Stinkerke@hotmail.com",
                LecturerEmail = "Swaggyteachy@school.be",
                LecturerNumber = "0696969696969",
                LecturerPicture = "https://pbs.twimg.com/profile_images/1872645193/swaggif.gif"
                
            };

            _unitOfWork.Repository<Teacher>().Insert(teacher);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }
        [TestMethod]
        public void UOW_Create_UserRole()
        {
            var userrole = new ApplicationUserRole
            {
                UserId = 8,
                RoleId = 1

            };

            _unitOfWork.Repository<ApplicationUserRole>().Insert(userrole);
            int result = _unitOfWork.SaveChanges();

            Assert.IsNotNull(result);
            Assert.AreNotEqual(-1, result);
        }

      

        [TestMethod]
        public void UOW_Create_Userstudent()
        {

            SHA1 sha1 = new SHA1CryptoServiceProvider();

            for (int i = 1; i < 6; i++)
            {
                string password = (Convert.ToString("!Student" + i + ""));
                var user = new ApplicationUser
                {
                    UserName = "Student" + i + "",
                    FirstName = "Voornaam_student" + i + "",
                    LastName = "Achternaam_student" + i + "",
                    Email = "Student" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),
                    ClassId = Convert.ToInt16(i)
                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersTeacher()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Teacher_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Teacher" + i + "",
                    FirstName = "Voornaam_Teacher" + i + "",
                    LastName = "Achternaam_Teacher" + i + "",
                    Email = "Teacher" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),
                    ClassId = Convert.ToInt16(i)
                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
             public void UOW_Create_UsersParent()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Parent_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Parent" + i + "",
                    FirstName = "Voornaam_Parent" + i + "",
                    LastName = "Achternaam_Parent" + i + "",
                    Email = "Parent" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),
                    ChildId = Convert.ToInt16(i)
                    
                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersTitular()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Titular" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Titular" + i + "",
                    FirstName = "Voornaam_Titular" + i + "",
                    LastName = "Achternaam_Titular" + i + "",
                    Email = "Titular" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),

                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersPrincipal()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Principal_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Principal" + i + "",
                    FirstName = "Voornaam_Principal" + i + "",
                    LastName = "Achternaam_Principal" + i + "",
                    Email = "Principal" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),

                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersCoordinator()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Coordinator_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Coordinator" + i + "",
                    FirstName = "Voornaam_Coordinator" + i + "",
                    LastName = "Achternaam_Coordinator" + i + "",
                    Email = "Coordinator" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),

                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersSecretary()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Secretary_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Secretary" + i + "",
                    FirstName = "Voornaam_Secretary" + i + "",
                    LastName = "Achternaam_Secretary" + i + "",
                    Email = "Secretary" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),

                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }
        [TestMethod]
        public void UOW_Create_UsersAdmin()
        {
            for (int i = 1; i < 6; i++)
            {
                var password = Convert.ToString("Admin_" + i + "");
                var user = new ApplicationUser
                {
                    UserName = "Admin" + i + "",
                    FirstName = "Voornaam_Admin" + i + "",
                    LastName = "Achternaam_Admin" + i + "",
                    Email = "Admin" + i + "@hotmail.com",
                    PasswordHash = password.GetHashCode().ToString(),

                };

                _unitOfWork.Repository<ApplicationUser>().Insert(user);
                int result = _unitOfWork.SaveChanges();

                Assert.IsNotNull(result);
                Assert.AreNotEqual(-1, result);
            }
        }

    }
}